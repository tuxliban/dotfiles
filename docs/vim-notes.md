## Vim Notes


### Search text

n search next
N searc previous

### Text replace

// sed syntax //
:s/text/new text/  change 1
:s/text/new text/g change all without confirm
:%s/text/new text/gc change all with confirm
// doing the same without nvim //
vim -c s/text/new text/g -c s/text/new text/g -c s/text/new text/g file.txt

### Splits

vim 02 file.1 file.2 --> 2 horizontal with files
vim O5  5 vertical


## Buffers

2 files  --> file.1 file2

:ls buffers list
:bn next buffer
:bp previous buffer
:qall  save and quit
:qall! quit without saving
:wqall save without quiting

