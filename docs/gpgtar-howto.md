## Encrypt / Decrypt directories with gpgtar

### Options
--encrypt   // -e      // encrypt 
--decrypt   // -d      // decrypt
--symmetric // -c      // asinging passphrase
--output    // -o      // ourput file name
--dry-run   //	       // just dry-run
--recipient user // -r user  // user
--tar-args  // --zstd		     // tar options

### Long options encrypt
gpgtar --output output-file.gpg --symmetric  input-dir




### Short options encrypt

gpgtar -o output-file.gpg -c input-dir


### Decrypt

gpg -d output-file.gpg
