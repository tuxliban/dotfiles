## Efi stub command to make efi entry with btrfs fs
doas efibootmgr --disk /dev/nvme0n1 --part 4 --create --label "Venom-Linux BTRFS"
--loader /vmlinuz-venom --unicode
'root=UUID=7edaf700-7ae7-4ae8-9c34-f4e1bd13875c rootfstype=btrfs
rootflags=subvol=@ rw initrd=\initrd-venom.img' --verbose
