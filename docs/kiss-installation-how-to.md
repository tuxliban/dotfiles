
= **How to install KISS Linux EFI** =

== **Preparations** ==

*  Use any live iso with tar and xz pgks, arch or void will do the trick
*  Download wget to download tarbal
*  It's recommended install using ssh to be able to follow the manual
*  Prepare your partitions and mount them in `/mnt`
*  Setting url variable `url=https://github.com/kisslinux/repo/releases/2021.7-9` to download kisslinux
* Download kisslinux `wget "$url/kiss-chroot-2021.7-9.tar.xz"`
* Verify the checksums with `wget "$url/kiss-chroot-2021.7-9.tar.xz.sha256"` and `sha256sum -c < kiss-chroot-2021.7-9.tar.xz.sha256`
* Extract kiss-chroot into /mnt with `cd /mnt && tar xvf /path/to/kiss-chroot-2021.7-0.tar.xz`
* Create fstab with arch=genfstab with `genfstab /mnt >> /mnt/etc/fstab`
* Enter chroot system `/mnt` with `/mnt/bin/kiss-chroot /mnt`

== **Setup Repositories** ==

* Edit kiss_path.sh to setup $KISS_PATH var
	`vi /etc/profile.d/kiss_path.sh`
* Add to kiss_path.sh :
	``` 
	export REPOS_DIR='var/db/kiss'
	export KISS_PATH=''
	
	KISS_PATH=$KISS_PATH:$REPOS_DIR/repo/core
	KISS_PATH=$KISS_PATH:$REPOS_DIR/repo/extra
	
	export CFLAGS="-O3 -pipe -march=native"
	export CXXFLAGS"$CFLAGS"
	export MAKEFLAGS="-j14"
	
	export KISS_SU=su

	```	
* Run kiss_path.sh script with `./etc/profile.d/kiss_path.sh`
* Check if it's alright with `echo $KISS_PATH`
* Cloning repos:
	* Change dir to repo dir with `cd /var/db/kiss`
	* Clone repos: `git clone https:/github.com/kisslinux/repo`
	* Enabling repo signing:
		* Install gnupg1 with `kiss build gnupg1 ; kiss install gnupg1`
		* Inport key `gpg --keyserver keyserver.ubuntu.com --recv-key 13295DAC2CF13B5C ; echo trusted-key 0x13295DAC2CF13B5C >>/root/.gnupg/gpg.conf` 
		* Verifying `cd /var/db/kiss/repo && git config merge.verifySignatures true`
		* Update system `kiss update`
		* Rebuild all pkgs `cd /var/db/kiss/installeda && kiss build *`
		
	
== **Installing necesary pkgs** ==

*In order to kiss build all pkgs you can do `kiss b $pkgs`, and you can skip `kiss i $pkgs`*

* Init scripts `kiss b baseinit`
* Grub `kiss b grub efibootmgr`
* Network `kiss b dhcpcd (wpa_supplicant 4 wifi networks)`
	* Setting network and services:
		```
		mkdir -p /etc/rc.d
		echo "dhcpcd 2> /dev/null" > /etc/rc.d/dhcpcd.boot
		echo $HOSTNAME > /etc/hostname
		echo "127.0.0.1 localhost.localdomain	localhost" >> /etc/hosts
		echo "::1 localhost.localdomain	localhost ip6-localhost" >> /etc/hosts
		echo "127.0.0.1 kisslinushost" >> /etc/hosts
	
		```
* Filesystems `kiss b e2fsprogs dosfstools`
* Devices `kiss b util-linux eudev`
* Kernel `kiss b libelf ncurses perl`
	* Download kernel in `cd /var/db/kiss/`
	* Download `wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.11.tar.xz`
	* Extract kernel `tar xvf linux-5.15.11.tar.xz && cd /linux-5.15*/`
	* Make kernel config `make localmodconfig`
	* Compile it `make -j14`
	* Install modules `make INSTALL_MOD_STRIP=1 modules_install`
	* Install kernel `make install`
	* Setting kernel files names `mv /boot/vmlinuz /boot/vmlinuz-VERSION && mv /boot/System.map /boot/System.map-VERSION`
* Grub:
	* Installing `grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=KissLinux-GRUB ` 
	* Creating config `grub-mkconfig -o /boot/grub/grub.cfg`
