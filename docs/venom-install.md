# Venom install guia

Pkgs="xz man-pages dosfstools e2fsprogs procps-ng nss ca-certificate dhcpcd kbd kmod xorg-server xauth xinit xf86-input-libinput xf86-video-amdgpu xrdb " 

 edita /etc/scratch.conf
export CFLAGS="-march=native -pipe -O2"
2) sincroniza # scratch sync
3) pon la nueva toolchain en /usr/ports/local (te voy a pasar un tar)
4) Pon el repo local en /etc/scratch.repo (importante que esté el primero, no tiene url porque lo sincronizamos manualmente) 
/usr/port/local
/usr/ports/main       https://github.com/venomlinux/ports/tree/3.0/main
#/usr/ports/multilib   https://github.com/venomlinux/ports/tree/3.0/multilib
#/usr/ports/nonfree    https://github.com/venomlinux/ports/tree/3.0/nonfree 
#/usr/ports/testing    https://github.com/venomlinux/ports/tree/3.0/testing
 5) Ejecuta el rebuild: # pkgrebuil
 
 
 Enter chroot
Chroot into the extracted venom image.

# mount -v --bind /dev /mnt/dev
# mount -vt devpts devpts /mnt/dev/pts -o gid=5,mode=620
# mount -vt proc proc /mnt/proc
# mount -vt sysfs sysfs /mnt/sys
# mount -vt tmpfs tmpfs /mnt/run
# mkdir -pv /mnt/$(readlink /mnt/venom/dev/shm)
# cp -L /etc/resolv.conf /mnt/etc/
# chroot /mnt /bin/bash


# grub-install /dev/sdX
# grub-mkconfig -o /boot/grub/grub.cfg
Note: replace 'X' with your partition drive

Exit chroot environment:

# exit
Unmount venom partition you mounted before:

# umount -v /mnt/dev/pts
# umount -v /mnt/dev
# umount -v /mnt/run
# umount -v /mnt/proc
# umount -v /mnt/sys
# umount /mnt
You can restart your machine now, Venom Linux should be bootable.

# reboot
