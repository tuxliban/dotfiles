# Void tarball installation

_This is a how-to make a tarball of your system and use it in a new installation._
_It'll have some options depeding of the file system to install to, ext4 or btrfs._

## Dependencies

	*  Bsdtar or any tar compression application
	*  Zstd library for compression, there's some other compressor but zstd have the best ratio / speed.



## Creating the tarball

_If your using btrfs, you can use the snapshots you'd have creadted to make the tarball_
_You can create it with:_

	`bsdtar -aczvf /path/to/output-file.zst /.snapshots/especific-snapshot/ `

 
_If your snapshot conteins also your home or you can exluce any directories or files with line .cache or .local, you can do it with this :_

	`--exclude="/path/to/dir"`

_One example could be:_

	`bsdtar -aczvf /path/to/output-file.zst --exclude="/path/to/dir" /.snapshots/especific-snapshot/ `


_If your using ext4, you can use any live iso to make the tarball_
	* Install the dependencies bsdtar/tar zstd
	* Mount the partitions
		- mount /dev/$root /mnt
		- mount /dev/$home /mnt/home
		- mount /dev/$boot /mnt/boot/efi

_Create the tarball with :_

	`bsdtar -aczvf /path/to/output-file.zst --exclude="/path/to/dir" /mnt/ `


## Deploing your tarball

### Partitioning

#### Btrfs

_We assumed your'll install in uefi mode_
_In this case we'll use:_
 * Boot partition --> 100M fat32 // we call it $boot
 * Root partition --> (You choose the size) btrfs // we call it $root
 * Home partition --> (You choose the size) btrfs // we call it $home
 * Snapshot partition --> (You choose the size) btrfs // we call it $snapshot
 
 #### Ext4
_We assumed your'll install in uefi mode_
_In this case we'll use:_
 * Boot partition --> 100M fat32 // we call it $boot
 * Root partition --> (You choose the size) ext4 // we call it $root
 * Home partition --> (You choose the size) ext4 // we call it $home
 
 
### Mounting Partitions
 
#### Btrfs

* Set the mount options (This are mines)
	- BTRFS_OPTS="rw,noatime,ssd,compress=zstd,space_cache=v2,commit=120"
* Mount $root partition
	- mount -o $BTRFS_OPTS /dev/$root /mnt
* Create @ subvolume
	- btrfs subvolume create /mnt/@
* Umount /mnt
	- umount /mnt
* Mount $home partition
	- mount -o $BTRFS_OPTS /dev/$home /mnt
* Create @home subvolume
	- btrfs subvolume create /mnt/@home
* Umount /mnt
	- umount /mnt
* Mount $snapshots partition
	- mount -o $BTRFS_OPTS /dev/$snapshot /mnt
* Create @snapshot subvolume
	- btrfs subvolume create /mnt/@snapshot
* Umount /mnt
	- umount /mnt
* Mounting @ subvol
	- mount -o $BTRFS_OPTS,subvol=@ /dev/$root /mnt
* Make dirs for the rest of subvols
	- mkdir -p /mnt/{.snapshots,home,var/cache,boot/efi}
* Mounting the rest of subvols
	- mount -o $BTRFS_OPTS,subvol=@snapshots /dev/$snapshos /mnt/.snapshots
	- mount -o $BTRFS_OPTS,subvol=@home /dev/$home /mnt/home
* Mounting $boot partition
	- mount /dev/$boot /mnt/boot/efi
* Creating subvols for avoid some dirs on the snapshots
	- btrfs subvolume create /mnt/var/cache/xbps
	- btrfs subvolume create /mnt/var/tmp
	- btrfs subvolume create /mnt/srv
	
#### Ext4

* Mounting $root partition
	- mount /dev/$root /mnt
* Making boot directories
	- mkdir -p /mnt/boot/efi
* Mounting $boot partition
	- mount /dev/$boot /mnt/boot/efi

### Extracting the tarball

`bsdtar -axvzf /path/to/tarball-file.zst /mnt/`

_If you have a separate home tarball extract it:_

`bsdtar -axvzf /path/to/tarball-file.zst /mnt/home/$USER`


### Configuring the new system

* Enter the new system using chroot
	- Btrfs
		- # for i in sys dev proc; do $(mount --rbind /$i /mnt/$i && mount --make-rslave /mnt/$i); done
		- # cp /etc/resolv.conf /mnt/etc/
	  	- # BTRFS_OPTS=$BTRFS_OPTS PS1='(chroot) # ' chroot /mnt/ /bin/bash
	- Ext4
		- # for i in sys dev proc; do $(mount --rbind /$i /mnt/$i && mount --make-rslave /mnt/$i); done
		- # cp /etc/resolv.conf /mnt/etc/
	  	- # PS1='(chroot) # ' chroot /mnt/ /bin/bash
* Edit the fstab with the new partitions UUID's
	- _Find the new UUID's with :_
		`blkid -s UUID -o value /dev/$boot`	
* Reinstall grub 
	- _Reinstall grub with :_
		`grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="void"``	
	- _If you're installing in a usb drive add this option to grub-install command_
		`--removable`
* If your're installing in a different machine if could be necesary to install some pkgs like wifi, gpu drivers, some firmwares, etc. Install them now :
		`xbps-install -Suy foo`
* Reconfigure all pkgs :
		`xbps-configure -fa`
