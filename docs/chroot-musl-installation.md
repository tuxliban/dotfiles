# Chroot musl UEFI installation process

### Luks encryption

 cryptsetup --cipher aes-xts-plain64 --hash sha512 --use-random --verify-passphrase luksFormat /dev/$root
 cryptsetup LuksOpen /dev/$root $cryptroot
 mkfs.btrfs --csum xxhash -L void /dev/mapper/cryptroot


### Disks partition

-  Crerate the filesystems.

	- # mkfs.vfat -F32 /dev/$boot
	- # mkfs.ext4 /dev/$root

- Mount both partitions.

	- # mount /dev/$root /mnt
	- # mkdir -p /mnt/boot/efi
	- # mount dev/$boot /mnt/boot/efi


### BTRFS FS option
	

#### Add this pkgs to install

	- btrfs-progs
	- grub-btrfs (to be able to boot with any snapshots)


### Formating and mounting

- Crerate the filesystems.

	- # mkfs.vfat -F32 /dev/$boot
	- # mkfs.btrfs --csum xxhash /dev/$root

- Creating de subvolumes and mounting partitions

	- # BTRFS_OPTS="rw,noatime,ssd,compress-force=zstd,space_cache=v2,commit=120"
	- # mount -o $BTRFS_OPTS /dev/$root /mnt
	- # btrfs subvolume create /mnt/@
	- # btrfs subvolume create /mnt/@home
	- # btrfs subvolume create /mnt/@snapshots
	- # umount /mnt
	- # mount -o $BTRFS_OPTS,subvol=@ /dev/$root /mnt
	- # mkdir -pv /mnt/{.snapshots,home,var/cache,boot/efi}
	- # mount /dev/$boot /mnt/boot/efi
	- # mount -o $BTRFS_OPTS,subvol=@snapshots /dev/$root /mnt/.snapshots
	- # mount -o $BTRFS_OPTS,subvol=@home /dev/$root /mnt/home
	- # btrfs subvolume create /mnt/var/cache/xbps
	- # btrfs subvolume create /mnt/var/tmp
	- # btrfs subvolume create /mnt/srv

- Using multiple partitions
	
	- # BTRFS_OPTS="rw,noatime,ssd,compress=zstd,space_cache=v2,commit=120"
	- # mount -o $BTRFS_OPTS /dev/$root /mnt
	- # btrfs subvolume create /mnt/@
	- # umount /mnt
	- # mount -o $BTRFS_OPTS /dev/$home /mnt
	- # btrfs subvolume create /mnt/@home
	- # umount /mnt
	- # mount -o $BTRFS_OPTS /dev/$snapshots /mnt
	- # btrfs subvolume create /mnt/@snapshots
	- # umount /mnt
	- # mount -o $BTRFS_OPTS,subvol=@ /dev/$root /mnt
	- # mkdir -p /mnt/{.snapshots,home,var/cache,boot/efi}
	- # mount -o $BTRFS_OPTS,subvol=@snapshots /dev/$snapshots /mnt/.snapshots
	- # mount -o $BTRFS_OPTS,subvol=@home /dev/$home /mnt/home
	- # btrfs subvolume create /mnt/var/cache/xbps
	- # btrfs subvolume create /mnt/var/tmp
	- # btrfs subvolume create /mnt/srv
	
	
### Base Installation    XBPS Method

- Setting mirror, arquitecture, packages variables

	- # REPO=https://alpha.de.repo.voidlinux.org/current/musl
	- # ARCH=x86_64-musl
	- # PKG="base-files>=0.77 ncurses coreutils gnupg findutils diffutils
	  libgcc dash grep gzip file sed gawk less util-linux which tar bsdtar
	  mdocml>=1.13.3 shadow dosfstools procps-ng tzdata pciutils usbutils
	  iana-etc openssh kbd iproute2 iputils wpa_supplicant xbps opendoas
	  traceroute ethtool kmod eudev runit-void removed-packages
	  ca-certificates dracut linux-firmware-amd make gcc pkg-config patch
	  grub-x86_64-efi dhcpcd neovim wget curl"

### Personal dwm instalation pkgs

	- # V_PKG="$PKG xorg-minimal xrdb lm_sensors xf86-video-amdgpu mesa-dri
	  mesa-vaapi udevil pulseaudio mpv xclip ffmpeg aria2 git qutebrowser
	  python3-adblock pdf.js"
	
- Installing PKGS
	- # XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" "$V_PKG"


### Configuration

- Entering the Chroot

	- Mount the pseudo-filesystems

		- # for i in sys dev proc; do $(mount --rbind /$i /mnt/$i && mount --make-rslave /mnt/$i); done
	
	- DNS configuration

		- # cp /etc/resolv.conf /mnt/etc/

	- Chroot into the new installation
	
	- # PS1='(chroot) # ' chroot /mnt/ /bin/bash

		- Chroot Btrfs 
	  
	  		- # BTRFS_OPTS=$BTRFS_OPTS PS1='(chroot) # ' chroot /mnt/ /bin/bash
		
	
	- Specify the hostname

		- (chroot) # echo "$user-musl" >> /etc/hostname
		
	- Configure rc.conf rc.local
	
	- Setting users and passwd
		
		* Set root passwd
			- (chroot) # passwd 
		* Set user groups and passwd 
			- (chroot) # useradd -m -G users,video,audio,network,storage,xbuilder,input $user
			- (chroot) # passwd $user
	
### Creating Btrfs fstab

	- # touch /mnt/etc/fstab
	- # (chroot) # UEFI_UUID=$(blkid -s UUID -o value /dev/$boot)
	- # (chroot) # ROOT_UUID=$(blkid -s UUID -o value /dev/$root)
	- # (chroot) # DATOS_UUID=$(blkid -s UUID -o value /dev/sda1 #(DATOS))
	- # (chroot) # TEKA_UUID=$(blkid -s UUID -o value /dev/sdb1 #(Visone-Teka))
	- # (chroot) #  cat <<EOF > /etc/fstab
	 UUID=$ROOT_UUID / btrfs $BTRFS_OPTS,subvol=@ 0 1
	 UUID=$UEFI_UUID /efi vfat noatime 0 2
	 UUID=$ROOT_UUID /home btrfs $BTRFS_OPTS,subvol=@home 0 2
	 UUID=$ROOT_UUID /.snapshots btrfs $BTRFS_OPTS,subvol=@snapshots 0 2
	 tmpfs /tmp tmpfs noatime,nosuid,nodev 0 0
	 UUID=$DATOS_UUID /media/Datos ext4 errors=remount-ro 0 0
	 UUID=$TEKA_UUID /media/Visone-Teka ext4 errors=remount-ro 0 0
	 //192.168.1.151/laptop /media/Laptop cifs username=$user,password=$passwd
	 EOF
	
### Creating Ext4 fstab
	
		- # touch /mnt/etc/fstab
		- # (chroot) # UEFI_UUID=$(blkid -s UUID -o value /dev/boot)
		- # (chroot) # ROOT_UUID=$(blkid -s UUID -o value /dev/root)
		- # (chroot) # DATOS_UUID=$(blkid -s UUID -o value /dev/sda1 #(DATOS))
		- # (chroot) # TEKA_UUID=$(blkid -s UUID -o value /dev/sdb1 #(Visone-Teka))
		- # (chroot) # cat <<EOF > /etc/fstab
		UUID=$UEFI_UUID /boot/efi vfat noatime 0 0
		UUID=$ROOT_UUID / ext4 noatime 0 0
		tmpfs           /tmp        tmpfs   noatime,nosuid,nodev   0 0
		UUID=$DATOS_UUID /media/Datos ext4 errors=remount-ro 0 0
		UUID=$TEKA_UUID /media/Visone-Teka ext4 errors=remount-ro 0 0
		//192.168.1.151/laptop /media/Laptop cifs username=$user,password=$passwd
		EOF
		
	- Configuring dracut.conf
	
		- (chroot) # echo "hostonly=yes" /etc/dracut.conf

	- Installing grub
	
		(chroot) # grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="$user-musl"	
		
	- Reconfigure pkgs
	
		(chroot) # xbps-reconfigure -fa
		
		
