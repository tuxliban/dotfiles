set -o allexport


case $- in
	*i*);;
	*) return;;
esac

XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
LESSHISTFILE="$XDG_CACHE_HOME"/less/history
GNUPGHOME="$XDG_CONFIG_HOME"/gnupg
GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
XDG_RUNTIME_DIR=/tmp/runtime-visone
#XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
PULSEMIXER_BAR_STYLE="╭╶╮╴╰╯◆◇· ──"
PATH="$HOME"/.local/bin/:/sbin:/usr/sbin:$PATH
PATH="$PATH:$(du "$HOME/scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
FZF_DEFAULT_OPTS="+s --layout=reverse --height 40% --multi --cycle --no-color --preview-window=border-none --info=hidden --bind 'alt-p:preview-page-up,alt-n:preview-page-down' --bind 'ctrl-a:select-all' --bind 'ctrl-s:toggle-sort' --bind 'ctrl-p:toggle-preview' "



# nnn Plugins

export NNN_PLUG_DIR='x:visone/vdelete-c;X:visone/vdelete-s;D:visone/mdownloads;T:visone/mtv-shows;O:visone/mothers;F:visone/mfilms;S:visone/mseries;v:diffs'
export NNN_PLUG_FILE='i:visone/viewer;z:-!devour mpv $nnn*'
export NNN_PLUG_VARIOS='f:fixname;d:dragdrop'
export NNN_PLUG_NVIM='e:-!nvim $nnn*'
export NNN_PLUG="$NNN_PLUG_DIR;$NNN_PLUG_FILE;$NNN_PLUG_VARIOS;$NNN_PLUG_NVIM"
export NNN_PLUG

# nnn Bookmarks

export NNN_BMS='w:~/wallpapers;d:/media/Datos/Downloads;t:/media/Datos/Downloads/Torrenting;s:~/scripts;o:/media/Datos/Varios/.others;S:/media/Datos/Varios/Series;F:/media/Datos/Varios/Films;V:/media/Datos/TV-Shows/'

# nnn options 
export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
export NNN_OPENER=/home/visone/.config/nnn/plugins/visone/vi-nuke
export NNN_USE_EDITOR=1                   
export NNN_OPTS="SceEuo"
export NNN_FIFO=/tmp/nnn.fifo 
PAGER="less -R~"               
EDITOR="vim"              
VISUAL="$PAGER"               
IMAGEVIEWER="display" 
FILE="nnn"
BROWSER='firefox'
LANG=es_ES.UTF-8
MENU="dmenu"
RES=$(sed 's/,/x/g' /sys/class/graphics/fb?/virtual_size)
export sel=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection


# nnn colors

BLK="04" CHR="04" DIR="04" EXE="00" REG="00" HARDLINK="00" SYMLINK="06" MISSING="00" ORPHAN="01" FIFO="0F" SOCK="0F" OTHER="02"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"

# Migration pass/pash
read -r PASH_KEYID < "$HOME/.config/password-store/.gpg-id"
export PASH_DIR=/media/Datos/linux/repo.git/Pass-ssh-station/password-store
export PASH_KEYID


# Use arrows to manage history

set -o emacs

alias __A=`echo "\020"`     # up arrow = ^p = back a command
alias __B=`echo "\016"`     # down arrow = ^n = down a command
alias __C=`echo "\006"`     # right arrow = ^f = forward a character
alias __D=`echo "\002"`     # left arrow = ^b = back a character
alias __H=`echo "\001"`     # home = ^a = start of line
alias __Y=`echo "\005"`     # end = ^e = end of line


# Auto startx

if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
	exec startx "$HOME"/.config/X11/xinitrc
fi
