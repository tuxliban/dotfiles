*foreground:        #737373
*background:        #0e1111
*.foreground:       #737373
*.background:       #0e1111
URxvt.foreground:   #737373
URxvt.background:   #0e1111
URxvt.cursorColor:  #737373
URxvt.borderColor:  #0e1111

! Colors 0-15.
*.color0: #0e1111
*color0:  #0e1111
*.color1: #666666
*color1:  #666666
*.color2: #8A8A8A
*color2:  #8A8A8A
*.color3: #9B9B9B
*color3:  #9B9B9B
*.color4: #A6A6A6
*color4:  #A6A6A6
*.color5: #CECECE
*color5:  #CECECE
*.color6: #D9D9D9
*color6:  #D9D9D9
*.color7: #ebebeb
*color7:  #ebebeb
*.color8: #a4a4a4
*color8:  #a4a4a4
*.color9: #666666
*color9:  #666666
*.color10: #8A8A8A
*color10:  #8A8A8A
*.color11: #9B9B9B
*color11:  #9B9B9B
*.color12: #A6A6A6
*color12:  #A6A6A6
*.color13: #CECECE
*color13:  #CECECE
*.color14: #D9D9D9
*color14:  #D9D9D9
*.color15: #ebebeb
*color15:  #ebebeb

! Black color that will not be affected by bold highlighting.
*.color66: #16120d
*color66:  #16120d

! Xclock colors.
XClock*foreground: #d4d9dd
XClock*background: #16120d
XClock*majorColor:  rgba:d4/d9/dd/ff
XClock*minorColor:  rgba:d4/d9/dd/ff
XClock*hourColor:   rgba:d4/d9/dd/ff
XClock*minuteColor: rgba:d4/d9/dd/ff
XClock*secondColor: rgba:d4/d9/dd/ff


! DPI
Xft.dpi:   96


!! URxvt Appearance
URxvt.font: xft:mononoki Nerd Font Mono:style=Regular:size=12
URxvt.boldFont: xft:mononoki Nerd Font Mono:style=Bold:size=12
URxvt.italicFont: xft:mononoki Nerd Font Mono:style=Italic:size=12
URxvt.boldItalicFont: xft:mononoki Nerd Font Mono:style=Bold Italic:size=12
URxvt.termName:rxvt-unicode-256color
Xft.antialias:  true
Xft.rgba:       rgb
Xft.hinting:    true
Xft.hintstyle:  hintsfull
Xft.autohint:   false
Xft.lcdfilter:  lcddefault
URxvt.internalBorder: 8
URxvt.letterSpace: 0
URxvt.lineSpace: 0
!URxvt.geometry: 92x24
!URxvt.cursorBlink: false
URxvt.cursorUnderline: false
URxvt.saveline: 2048
URxvt.scrollBar: false
URxvt.scrollBar_right: false
URxvt.urgentOnBell: true
URxvt.depth: 32
URxvt.inheritPixmap: true
URxvt.iso14755: false
URxvt.print-pipe: "cat > /dev/null"

!! Common Keybinds for Navigations
URxvt.keysym.Shift-Up: command:\033]720;1\007
URxvt.keysym.Shift-Down: command:\033]721;1\007
URxvt.keysym.Control-Up: \033[1;5A
URxvt.keysym.Control-Down: \033[1;5B
URxvt.keysym.Control-Right: \033[1;5C
URxvt.keysym.Control-Left: \033[1;5D

!! Perl Extensions
URxvt.perl-ext-common:default,selection-to-clipboard,keyboard-select,font-size,tabbedalt,url-select-plus,-confirm-paste,vtwheel
URxvt.clipboard.autocopy: true
URxvt.copyCommand: xsel -b 
URxvt.pasteCommand: xsel -o
URxvt.keysym.Control-V: eval:paste_clipboard
URxvt.keysym.Control-C: eval:selection_to_clipboard
URxvt.keysym.Control-Escape: perl:keyboard-select:activate
URxvt.keysym.Control-s: perl:keyboard-select:search
URxvt.keysym.Control-u: perl:url-select:select_next
URxvt.keysym.M-u: perl:url-seleca-true:select_next
URxvt.url-select-plus.launcher: /home/visone/scripts/vi-nuke
URxvt.url-select-plus.altlauncher: firefox 
URxvt.url-select-plus.underline: true
URxvt.url-select-plus.autocopy: true
URxvt.url-select-plus.mediaplayer: mpv
URxvt.url-select-plus.imgviewer: display
URxvt.underlineURLs: true
URxvt.urlButton: 1
URxvt.keysym.Alt-plus: font-size:increase
URxvt.keysym.Alt-minus: font-size:decrease
URxvt.keysym.S-plus: font-size:incglobal
URxvt.keysym.S-minus: font-size:decglobal
URxvt.keysym.Alt-equal: font-size:reset
URxvt.keysym.Alt-slash: font-size:show

!TABS
URxvt.tabbedalt.autohide: true
URxvt.tabbedalt.tabbar-fg: 3
URxvt.tabbedalt.tabbar-bg: 0
URxvt.tabbedalt.tab-fg:    6
URxvt.tabbedalt.tab-bg:    0
URxvt.tabbedalt.active-bg: 0
URxvt.tabbedalt.actives-fg: 9
URxvt.tabbedalt.new-button: false
URxvt.tabbedalt.tab-numbers: false
URxvt.tabbedalt.tabcmds.1: S|shell
URxvt.tabbedalt.tabcmds.2: F|nnn|nnn
URxvt.tabbedalt.tabcmds.3: N|vim|vim
!URxvt.tabbedalt.session: F|S
