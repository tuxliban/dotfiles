" Visone nvim config


" Open fold cursor --> zo
" Close fold cursor --> zc
" Open all folds   --> zN
" Close all folds  --> zM


source $HOME/.config/nvim/vim-plug/plugins.vim


" SETTINGS --------------------------------------------------------------- {{{

syntax enable
set t_Co=256
let base16colorspace=256
set conceallevel=0
set hidden
set smartindent
set autoindent
set nobackup
set nowritebackup
set clipboard=unnamed
set clipboard=unnamedplus
set number
set title
set mouse=a
set cursorline
"set termguicolors
set spelllang=en,es
colorscheme monochrome
set wildmenu
set lazyredraw
set showmatch
au CursorHoldI * stopinsert
set ruler
set splitright
set splitbelow
set completeopt+=noinsert
set completeopt+=noselect



" }}}


" MAPPINGS --------------------------------------------------------------- {{{

let mapleader = ","
let @1="i#! /bin/sh2o"
let @2="Da.mkv\<Esc>"
let @3="Da.srt\<Esc>"
let @r="rv\<Esc>"
let @e="ev\<Esc>"
let @h=",bh:e\<Esc>"
let @v=",bv:e\<Esc>"

" keybindings

noremap <silent> <leader>bb <C-^> "switch between current and last buffer
nnoremap <silent> <leader>bh :new<CR> " horizontal split new buffer
nnoremap <silent> <leader>bv :vnew<CR> " vertical split new buffer
nnoremap <C-p> :Files<CR>
nnoremap ev :e ~/.config/nvim/init.vim<CR>
nnoremap rv :source ~/.config/nvim/init.vim<CR>

" copy/paste
"visual mode copy to clipboard
vnoremap Y "+y     
"visual mode paste clipboard
vnoremap P "+p 
"normal mode copy to end of the line
nnoremap Y y$       



"move/resize windows
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>
nmap <C-f> :NERDTreeToggle<CR>
nnoremap <silent> <c-Up> :resize -1<CR>
nnoremap <silent> <c-Down> :resize +1<CR>
nnoremap <silent> <c-left> :vertical resize -1<CR>
nnoremap <silent> <c-right> :vertical resize +1<CR>

" Save & quit
noremap Q :q<CR>
noremap <C-q> :qa<CR>
noremap S :w<CR>


" move start/end line
noremap <silent> N 0
noremap <silent> I $

" move lines
nnoremap <A-j> :m .+1<CR>== 
nnoremap <A-k> :m .-2<CR>== 
inoremap <A-j> <Esc>:m .+1<CR>==gi 
inoremap <A-k> <Esc>:m .-2<CR>==gi 
vnoremap <A-j> :m '>+1<CR>gv=gv 
vnoremap <A-k> :m '<-2<CR>gv=gv


" Insert key
noremap k i
noremap K I


" tab management
noremap tt :tabnew<CR>
noremap tp :-tabnext<CR>
noremap tn :+tabnext<CR>
noremap tc :tabc<CR>
noremap tmp :-tabmove<CR>
noremap tmn :+tabmove<CR>


"Startify
noremap h :Startify<CR>

" Qutebrowser links
nmap gx :silent execute "!qutebrowser " . shellescape("<cWORD>") . " &"<CR>

" }}}


" VIMSCRIPT -------------------------------------------------------------- {{{

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" More Vimscripts code goes here.

" }}}


" STATUS LINE ------------------------------------------------------------ {{{


" }}}
