" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

	" Auto pairs for '(' '[' '{'
	Plug 'jiangmiao/auto-pairs'
	" VimWiki
	Plug 'vimwiki/vimwiki'
	" Vim-monochorme
	Plug 'fxn/vim-monochrome'
	" nvim startpage
	Plug 'mhinz/vim-startify'
	" show hex colors
	Plug 'norcalli/nvim-colorizer.lua'

call plug#end()

" vimwiki settings
" visone-wiki

let visone = {}
let visone.path = '~/viwiki/visone'
let visone.path_html = '~/viwiki/visone/visone-html'
let visone.template_path = '~/viwiki/templates/'
let visone.template_default = 'def_template'
let visone.template_ext = '.html'
let visone.ext = '.md'
let visone.auto_export = 1
let visone.auto_toc = 1

" manjarolinuxes-wiki

let manjaro = {}
let manjaro.path = '~/viwiki/manjaro'
let manjaro.path_html = '~/viwiki/manjaro/manjaro-html'
let manjaro.template_path = '~/viwiki/templates/'
let manjaro.template_default = 'def_template'
let manjaro.template_ext = '.html'
let manjaro.ext = '.md'
let manjaro.auto_export = 1

let g:vimwiki_list = [visone, manjaro]

" add the pre tag for inerting code snippets

let g:vimwiki_valid_html_tags = 'b,i,s,u,sub,sup,kbd,br,hr,pre,script'

" startify options

let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ { 'type': 'commands',  'header': ['   Commands']       },
          \ ]
	
let g:startify_bookmarks = [ 
			    \{'k': '~/.mkshrc'}, 
			    \{'p': '~/.profile'}, 
			    \{'x': '~/.Xresources'}, 
			    \{'s': '~/scripts/'},
			    \{'b': '~/.config/qutebrowser/'},
			    \{'u': '~/.config/qutebrowser/userscripts/'},
			    \]


let g:startify_files_number = 5
let g:startify_update_oldfiles = 0
let g:startify_change_to_dir = 1
let g:startify_padding_left = 10
let g:startify_fortune_use_unicode = 0
let g:startify_custom_header =  'startify#pad(startify#fortune#boxed())'
		

" colorize
set termguicolors
lua require'colorizer'.setup()
