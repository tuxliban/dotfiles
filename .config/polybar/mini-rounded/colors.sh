#!/bin/bash

[color]

#; Use pywal.sh in scripts directory to use colors from an image/wallpaper.

.; main colors
background = #181611
foreground = #181611
foreground-alt = #efdfcc
alpha = #00000000

.; shades
shade1 = #95856C
shade2 = #AD8D6D
shade3 = #95856C
shade4 = #AD8D6D
shade5 = #AD8D6D
shade6 = #AD8D6D
shade7 = #95856C
shade8 = #AD8D6D

.; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
