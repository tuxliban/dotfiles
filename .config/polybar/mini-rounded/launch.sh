#!/usr/bin/env bash

# Add this script to your wm startup file.

DIR="$HOME/.config/polybar/mini-rounded"

# Terminate already running bar instances
#killall -q polybar
pkill  polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

i3() {
# Launch i3wm bar
polybar -q main -c "$DIR"/i3-config.ini &
#polybar -q title -c "$DIR"/i3-config.ini &
}

bspwm() {
# launch bspwm bar
polybar -q main -c "$DIR"/bspwm-config.ini &
polybar -q title -c "$DIR"/bspwm-config.ini &
}



case "$1" in

	-i) i3 ;;
	-b) bspwm ;;
esac

