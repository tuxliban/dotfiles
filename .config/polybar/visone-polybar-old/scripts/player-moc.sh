#!/bin/sh

if [ "$(padsp mocp -Q %state)" != "STOP" ];then
    SONG=$(padsp mocp -Q %song)
        
    if [ -n "$SONG" ]; then
        echo "$SONG - $(padsp mocp -Q %album)"
    else
        basename "$(padsp mocp -Q %file)"
    fi
else
    echo ""
fi
