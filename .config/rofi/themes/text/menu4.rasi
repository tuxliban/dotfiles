/*
 *
 * Author  : Aditya Shakya
 * Mail    : adi1090x@gmail.com
 * Github  : @adi1090x
 * Twitter : @adi1090x
 *
 */

configuration {
    lines:							15;
    columns:						1;
    font: 							"Iosevka Nerd Font 12";
    bw: 							0;
    location: 						1;
    padding: 						0;
    fixed-num-lines: 				true;
    show-icons: 					false;
    sidebar-mode: 					true;
    separator-style: 				"none";
    hide-scrollbar: 				true;
    fullscreen: 					false;
    fake-transparency: 				false;
    scroll-method: 					0;
    window-format: 					"[{w}] ··· {c} ···   {t}";
    click-to-exit: 					true;
    show-match: 					false;
    combi-hide-mode-prefix: 		false;
    display-window: 				"";
    display-windowcd: 				"";
    display-run: 					"";
    display-ssh: 					"";
    display-drun: 					"";
    display-combi: 					"";
}

@import "/home/visone/.cache/wal/colors-rofi-dark.rasi"

* {
    background:             	transparent;
}

window {
    border: 						2px;
    border-color: 				@selected-active-background;
    border-radius: 					10px;
    padding: 						30;
    width: 							20%;
    height: 						70%;
    location: west;
    x-offset: 150px; 
}

prompt {
    spacing: 						0;
    border: 						2px;
    text-color: 					@normal-foreground;
}

textbox-prompt-colon {
    expand: 						false;
    str: 							" ";
    margin:							0px 4px 0px 0px;
    text-color: 					inherit;
}

entry {
    spacing:    					0;
    text-color: 					@normal-foreground;
}

case-indicator {
    spacing:    					0;
    text-color: 					@accent;
}

inputbar {
    spacing:    					0px;
    text-color: 					@urgent-foreground;
    padding:    					1px;
    children: 						[ prompt,textbox-prompt-colon,entry,case-indicator ];
}

mainbox {
    border: 						2px;
    border-color: 					@accent;
    padding: 						6;
}

listview {
    fixed-height: 					0;
    border: 						2px;
    border-color: 					@accent;
    spacing: 						4px;
    scrollbar: 						false;
    padding: 						15px 5px 0px 5px;
}

element {
    border: 						2px;
    border-radius: 					10px;
    padding: 						5px;
}
element normal.normal {
    background-color: 				@normal-background;
    text-color:       				@normal-foreground;
}
element normal.urgent {
    background-color: 				@urgent-background;
    text-color:       				@urgent-foreground;
}
element normal.active {
    background-color: 				@active-background;
    text-color:       				@active-foreground;
}
element selected.normal {
    background-color: 				@selected-normal-background;
    text-color:       				@selected-normal-foreground;
}
element selected.urgent {
    background-color: 				@selected-urgent-background;
    text-color:       				@selected-urgent-foreground;
}
element selected.active {
    background-color: 				@selected-active-background;
    text-color:       				@selected-active-foreground;
}
element alternate.normal {
    background-color: 				@normal-background;
    text-color:       				@normal-foreground;
}
element alternate.urgent {
    background-color: 				@normal-background;
    text-color:       				@normal-foreground;
}
element alternate.active {
    background-color: 				@active-background;
    text-color:       				@active-foreground;
}

sidebar {
    border:       					2px;
    border-color: 					@accent;
    border-radius: 					10px;
}

button {
    background-color:             	@normal-background;
    margin: 						5px;
    padding: 						5px;
    text-color: 					@urgent-foreground;
    border: 						2px;
    border-radius: 					10px;
    border-color: 					@accent;
}

button selected {
    background-color:             	@active-background;
    text-color: 					@selected-active-foreground;
    border: 						2px;
    border-radius: 					10px;
    border-color: 					@selected-active-foreground;
}

scrollbar {
    width:        					4px;
    border:       					2px;
    handle-color: 					@accent;
    handle-width: 					8px;
    padding:      					0;
}

message {
    border: 						0px;
    border-color: 					@accent;
    padding: 						1px;
}

textbox {
    text-color: 					@normal-foreground;
}
