#!/bin/bash

[color]

#; Use pywal.sh in scripts directory to use colors from an image/wallpaper.

.; main colors
background = #0A0A0A
foreground = #0A0A0A
foreground-alt = #ebebeb
alpha = #00000000

.; shades
shade1 = #666666
shade2 = #8A8A8A
shade3 = #666666
shade4 = #8A8A8A
shade5 = #8A8A8A
shade6 = #8A8A8A
shade7 = #666666
shade8 = #8A8A8A

.; _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
