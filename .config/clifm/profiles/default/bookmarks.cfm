### This is the bookmarks file for clifm ###

# Empty and commented lines are ommited
# The bookmarks syntax is: [shortcut]name:path
# Example:
[c]clifm:/home/visone/.config/clifm/profiles/default
[sc]scripts:/home/visone/scripts/
[dd]datos:/media/Datos/
[ddt]datos:/media/Datos/Downloads/Torrenting/
[dw]downloads:/media/Datos/Downloads/
[dl]linux:/media/Datos/linux/
[dr]repo:/media/Datos/linux/repo.git/
[drv]void:/media/Datos/linux/repo.git/void-packages/
[dvv]venom:/media/Datos/linux/repo.git/forked-repos/venomlinux-visone-ports/
[dvs]varios:/media/Datos/Varios
[dvss]varios:/media/Datos/Varios/Series/
[dvsf]varios:/media/Datos/Varios/Films/
[dvso]varios:/media/Datos/Varios/.others/

