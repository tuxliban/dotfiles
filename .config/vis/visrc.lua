require('vis')


-- Plugins
-- require('plugins/vis-vim-compatibility-pack/vis-vim-compatible')
require('plugins/complete-filename')
require('plugins/title')
require('plugins/filetype')
require('plugins/quickfix')
require('plugins/tmux-rpel')
require('plugins/sneak')
require('plugins/highlight')
-- hi.patterns[' +\n'] = { style = 'back:#444444', hideOnInsert = true }
-- hi.patterns['hi'] = { style = 'back:gray,fore:blue,underlined:true,bold:true' }

-- Opciones de configuración global
vis.events.subscribe(vis.events.INIT, function()
    vis:command('set change-256colors')
    vis:command('set theme minimal-dark')
end)

-- Opciones de configuración por ventana
vis.events.subscribe(vis.events.WIN_OPEN, function(win)
    vis:command('set cursorline')
    vis:command('set number')
    vis:command('set autoindent')
    vis:command('set ignorecase')
    vis:command('set tabwidth 4')
    vis:command('set expandtab')
end)
