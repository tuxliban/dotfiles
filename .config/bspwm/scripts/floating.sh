#!/bin/bash

# Make a desktop floating


FLOATING_DESKTOP_ID=$(bspc query -D -d '^4')
desk_id=$(bspc query -D -d "${5:-focused}")

[ "$FLOATING_DESKTOP_ID" = "$desk_id" ] && echo "state=floating"

