
= *FFMPEG Commands* =


== Ffmpeg transcode vaapi hevc ==

This options are based in rx 550 amd gpu
	

=== Options ===


==== Hardware accelration ====
	
	`-hwaccel vaapi -vaapi_device`

==== Device ====
	
	`/dev/dri/renderD128`

==== Video Format ====
	
	`-vf 'format=nv12,hwupload'`

==== Video Codec ====
	
	`-c:v hevc_vaapi`

==== Video Bitrate / max bitrate ====
	
	`-b:v 600k -maxrate 800k`

==== Audio Codec ====
	
	`-c:a aac`

==== Audio Format ====
	
	`-af 'volume=2'`

==== Audio Bitrate ====
	
	`-b:a 160k`

==== Varios options ====

	* `y` --> `overwrite output file if exist`


=== Example ===


* _*Use this command to transcode a video using the gpu vaapi capabilities*_

* _*Using video codec hevc 8bits with 600k bitrate and 800 maxrate*_ 

* _*Audio codec aac with 160k bitrate*_

* _*Normalize and dobled volume*_

`ffmpeg -hwaccel vaapi -vaapi_device /dev/dri/renderD128 -i input-file.mkv -vf "format=nv12,hwupload" -c:v hevc_vaapi  -b:v 600k -maxrate 800k -c:a aac -af "volume=2" -b:a 160k -y output-file.hevc.mkv` 

