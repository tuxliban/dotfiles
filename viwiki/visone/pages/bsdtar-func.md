
= *Bsdtar extract/compress functions* =

_Copy this functions to your shell config file (.bashrc,.zshrc,etc)_



== Dependencies: ==

	* Bsdtar


=== Code: ===

{{{


# visone bsdtar functions

extract() {
 if [ -f "$1" ]; then
  case $1 in
   *.tar.bz2|*.tbz)
    bsdtar -xvjf "$1"
    ;;
   *.tar.gz|*.tgz)
    bsdtar -xvzf "$1"
    ;;
   *.tar.xz|*.txz)
    bsdtar -xvjf "$1"
    ;;
   *.tar.zst|*.zst)
    bsdtar -axvzf "$1"
    ;;
   *.rar)
    bsdtar -xf "$1"
    ;;
   *.zip)
    bsdtar -xf "$1"
    ;;
   *.7z)
    bsdtar -xf "$1"
    ;;
   *)
    print "'$1': unknown format"
    print "It couldn't extract the file"
    print "Usage:\n"
    print "extract file.<format>\n"
    return
    ;;
  esac
 else
    print "'$1': unknown format"
    print "Usage:\n"
    print "extract file.<format>\n"
    return
 fi
}

compress() {

file="$1"
shift
  case $file in
   *.tar)
    bsdtar -cvf "$file" "$@"
    ;;
   *.tar.gz|*.tgz)
    bsdtar -czvf "$file" "$@"
    ;;
   *.tar.xz|*.txz)
    bsdtar -cvJf "$file" "$@"
    ;;
   *.tar.zst|*.zst)
    bsdtar  -aczvf "$file" "$@"
    ;;
   *.rar)
    bsdtar -cf "$file" "$@"
    ;;
   *.zip)
    bsdtar -cf "$file" "$@"
    ;;
   *.7z)
    bsdtar -cf "$file" "$@"
    ;;
   *)
	print "It couldn't copress them\n"
      	print "Usage:\n"
      	print "compress file.<format> foo1 foo2 fooN\n"
      	return
      ;;
  esac

}


}}}
