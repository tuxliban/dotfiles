
== *URxvt launcher* ==

_*This scripts funtion as launcher for rxvt-unicode terminal assinging position.*_
_*I wrote it to use it in dwm, so adding a wm rule to launch any instance of st
in floating mode, you can set the position of the window.*_

=== *Dependencies: ===

	*  rxvt-unicode terminal
	*  nnn file manager
	*  pulsemixer (check wiki)
	*  fzf
	*  remind
	*  telegram-tg
	
=== Code: ===

{{{

#!/bin/sh

# Usage: 
# urxvt-launcher $OPT

case $1 in
	## scratchpads
	-s) urxvtc -name rxvt ;;
	-g) urxvtc -name tg -e tg ;;
	-r) urxvtc -name float -g 189x10+10+865 ;;
	-p) urxvtc -name pulsemixer -g 189x10+10+865 -e pulsemixer ;;
	## no-scratchpads
	-v) urxvtc -name term -g 59x10+650+865 -e fzvarios ;;
	-f) urxvtc -name term -g 59x10+650+865 -e fzfilms ;;
	-t) urxvtc -name term -g 59x10+650+865 -e fztvshow ;;
	-z) urxvtc -name term -g 79x10+650+865 -e vi-fzf ;;
	-d) urxvtc -name term -g 89x10+650+865 -e vi-fzdragon ;;
	-m) urxvtc -name term -g 39x10+750+865 -e fzf-menu ;;
	-x) urxvtc -name term -g 39x10+750+865 -e void-session ;;
	-k) urxvtc -name float -g 89x30+400+150 -e key-spectrwm ;;
	-c) urxvtc -name float -g 69x10+1200+20 -e calendar ;;
esac


}}}
