
=*Dwmblocks System Script*=

 _Visone System Monitor Script_
 
== Description ==
 
This scripts allows you to show used Ram, % cpu used,free space in your / partition 


== Code: ==

 {{{
 
#! /bin/bash 

mem="$(free -h | awk '/^Mem:/ {print $3}')"
cpu="$(top -bn 1 | awk '/^%Cpu/ {print int($2 + $4 + $6)" %"}')"
fs=$(df -h / | awk ' /[0-9]/ {print $4}')
echo -e "└ $mem / $cpu / $fs ┘"

}}}
