
= *Mksh Conf* =


_*Visone mksh shell config with history*_


* _*Make the file*_ `~/.mksh_history`
* _*Make the file*_ `~/.mkshrc` _*with this*_



== Code: ==


{{{

# Alias
[ -r ~/.aliases ] && . ~/.aliases



if [ "$USER" = "$(id -un)"  ]
 then
  HISTFILE="$HOME/.mksh_history"
  HISTSIZE=5000
 else
  HISTFILE="/.mksh_history"
  HISTSIZE=5000
fi

# pwd
get_dir() {
	    pwd | sed "s|^$HOME|\n{V}|" | sed "s|^/media/Datos|\n{D}|" | sed "s|^/media/Visone-Teka|\n{T}|"; print "{$USER}"
}

git_branch() {
	BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
	[ ! "${BRANCH}" = "" ] && printf '%s' "{ Branch ${BRANCH} }"
	unset BRANCH
}
#PS1=$'$(prompt)$(git_branch) ❯❯❯❯ '
#PS1=$'$(prompt)$(git_branch)   '
PS1=$'$(get_dir)$(git_branch) ❯ '
man() {
	LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	LESS_TERMCAP_md=$(printf "\e[1;31m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;32m") \
	command man "$@"
}


# xbps-dmenu functions

ins() { 

	op="$(xbps-query -Rs '*' | awk '{print $2}' | dmenu -b -X 260 -Y 15 -W 1400 -p "Select package{s} to install: ")"
	[ "$op" != ""  ] && doas xbps-install -Su $op
}


del() {

	op="$(xbps-query -m | dmenu -b -X 260 -Y 15 -W 1400 -p "Select package{s} to remove: ")" 
	[ "$op" != ""  ] && doas xbps-remove -RcOon $op
}

up() {
	pkg=$(xbps-install --memory-sync --dry-run --update | grep -Fe update -e install | awk '{print $1}')
	if	[ "$pkg" != "" ]
		then
			printf "\t\t Void Package{s} to update:\n\n$pkg\n\n"
			doas xbps-install -Su
			doas xbps-remove -oO 
			doas rm -r /var/cache/xbps/*
		else
			printf "\t\t Void Package{s} to update:\n\n\t\t----- Nothing to Update -----\n\n" 
	fi
}


vcd() {

if  [ "$1" != "" ] 
    then
	cd "$1"
    else
	 sel="$( ls -A | fzf)"
	 if [ -d "$sel" ]
	    then
		cd "$sel"
	 elif [ -f "$sel" ]
	    then
		vi-nuke "$sel"  
	 fi 
    fi
}

# FZF funtions




se() {
   op="$(find "$HOME/scripts/" -type f | awk '!/Original-Scripts/ && !/README/' | fzf --delimiter / --with-nth -1 --prompt="Select Script to edit: " --preview='head -$LINES {}' --height=30% --margin=0%,39%,0%,0%)"
   [ -f "$op"  ] && $EDITOR $op ; clear
}

ce() {
op="$(find $HOME/.config/ -type f | awk  '!/watch_later/ && !/playlist/ && !/autoload/' | fzf --delimiter / --with-nth -1 --prompt=" Select Config File to edit:  " --preview='head -$LINES {}' --height=30%ai)" ; [ -f "$op" ] && $EDITOR $op ; clear ;
}


fw() {
op="$(find $HOME/viwiki/ -name *.md | fzf --delimiter / --with-nth -1 --prompt=" Select Viwiki File to edit:  " --preview='head -$LINES {}' --height=30%)" ; [ -f "$op" ] && $EDITOR $op ; clear
}

fins() { 

op="$(xbps-query -Rs '*' | awk '{print $2}' | fzf  --prompt="Select package{s} to install: " --margin=0%,49%,0%,0% --preview-window hidden)"
[ "$op" != "" ] && doas xbps-install -Su $op
}


fdel() {

op="$(xbps-query -m | fzf --prompt="Select package{s} to remove: " --margin=0%,49%,0%,0% --preview-window hidden)"
[ "$op" != "" ] && doas xbps-remove -RcOon $op
}

fscins() {

local=/usr/ports/local
main=/usr/ports/main
op="$(ls -A $local $main  | sort -u | fzf --delimiter / --with-nth -1 --prompt="Select package{s} to install: " --margin=0%,49%,0%,0% --preview-window hidden)"
[ "$op" != "" ] && doas scratch install  "$op"
}


fscdel() {

op="$(ls $(pkgadd --print-dbdir) | fzf --delimiter / --with-nth -1 --prompt="Select package{s} to remove: " --margin=0%,49%,0%,0% --preview-window hidden)"
[ "$op" != "" ] && doas scratch remove "$op"
}

fzh() {
  a=$( history | sed 's/.[ ]*.[0-9]*.[ ]*//' | perl -ne 'print if !$seen{$_}++' | fzf --preview-window hidden --margin=0%,49%,0%,0% --tac -i --no-sort )
#   echo $a
   eval $a
  }


# gpgtar function

gcompress(){
    pkill gpg-agent
    gpgtar -o ${1%.zst}.gpg -r gpgtar -e $1
}

gextract() {
    pkill gpg-agent
    gpgtar -d "$1"
}




# visone bsdtar functions

extract() {
 if [ -f "$1" ]; then
  case $1 in
    *.tar)
    bsdtar -xvf  "$1"
    ;;
   *.tar.bz2|*.tbz)
    bsdtar -xvjf "$1"
    ;;
   *.tar.gz|*.tgz)
    bsdtar -xvzf "$1"
    ;;
   *.tar.xz|*.txz)
    bsdtar -xvjf "$1"
    ;;
   *.tar.zst|*.zst)
    bsdtar -xvzf "$1"
    ;;
   *.rar)
    bsdtar -xf "$1"
    ;;
   *.zip)
    bsdtar -xf "$1"
    ;;
   *.7z)
    bsdtar -xf "$1"
    ;;
   *)
    print "'$1': unknown format"
    print "It couldn't extract the file"
    print "Usage:\n"
    print "extract file.<format>\n"
    return
    ;;
  esac
 else
    print "'$1': unknown format"
    print "Usage:\n"
    print "extract file.<format>\n"
    return
 fi
}

compress() {

file="$1"
shift
  case $file in
   *.tar)
    bsdtar -cvf "$file" "$@"
    ;;
   *.tar.gz|*.tgz)
    bsdtar -czvf "$file" "$@"
    ;;
   *.tar.xz|*.txz)
    bsdtar -cvJf "$file" "$@"
    ;;
   *.tar.zst|*.zst)
    bsdtar  -aczvf "$file" "$@"
    ;;
   *.rar)
    bsdtar -cf "$file" "$@"
    ;;
   *.zip)
    bsdtar -cf "$file" "$@"
    ;;
   *.7z)
    bsdtar -cf "$file" "$@"
    ;;
   *)
	print "It couldn't copress them\n"
      	print "Usage:\n"
      	print "compress file.<format> foo1 foo2 fooN\n"
      	return
      ;;
  esac

}

}


}}}


* _*Make the file*_ `~/.profile` _*with this:*_


== Code: ==


{{{

set -o allexport


case $- in
	*i*);;
	*) return;;
esac

#ENV=$HOME/.kshrc
XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
LESSHISTFILE="$XDG_CACHE_HOME"/less/history
GNUPGHOME="$XDG_CONFIG_HOME"/gnupg
GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
XDG_RUNTIME_DIR=/tmp/runtime-visone
XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
PULSEMIXER_BAR_STYLE="╭╶╮╴╰╯◆◇· ──"
PATH="$HOME"/.local/bin/:/sbin:/usr/sbin:$PATH
PATH="$PATH:$(du "$HOME/scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
FZF_DEFAULT_OPTS="+s --layout=reverse --height 40% --multi --cycle --no-color --border=rounded --info=hidden --bind 'alt-p:preview-page-up,alt-n:preview-page-down' --bind 'ctrl-a:select-all' --bind 'ctrl-s:toggle-sort' --bind 'ctrl-p:toggle-preview' "



# nnn Plugins

export NNN_PLUG_DIR='x:visone/vdelete-c;X:visone/vdelete-s;D:visone/mdownloads;T:visone/mtv-shows;O:visone/mothers;F:visone/mfilms;S:visone/mseries;v:diffs'
export NNN_PLUG_FILE='i:visone/viewer;z:-!devour mpv $nnn*'
export NNN_PLUG_VARIOS='f:fixname;d:dragdrop'
export NNN_PLUG_NVIM='e:-!nvim $nnn*'
export NNN_PLUG="$NNN_PLUG_DIR;$NNN_PLUG_FILE;$NNN_PLUG_VARIOS;$NNN_PLUG_NVIM"
export NNN_PLUG

# nnn Bookmarks

export NNN_BMS='w:~/wallpapers;d:/media/Datos/Downloads;t:/media/Datos/Downloads/Torrenting;s:~/scripts;o:/media/Datos/Varios/.others;s:/media/Datos/Varios/Series;f:/media/Datos/Varios/Films;t:/media/Datos/TV-Shows/'

# nnn options 
export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
export NNN_OPENER=/home/visone/.config/nnn/plugins/visone/vi-nuke
export NNN_USE_EDITOR=1                   
export NNN_OPTS="SceEuo"
export NNN_FIFO=/tmp/nnn.fifo 
PAGER="less -R~"               
EDITOR="vim"              
VISUAL="$PAGER"               
IMAGEVIEWER="display" 
FILE="nnn"
BROWSER='firefox'
#TERM="urxvtc"
LANG=es_ES.UTF-8
MENU="dmenu"
RES=$(sed 's/,/x/g' /sys/class/graphics/fb?/virtual_size)
export sel=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection


# nnn colors

BLK="04" CHR="04" DIR="04" EXE="00" REG="00" HARDLINK="00" SYMLINK="06" MISSING="00" ORPHAN="01" FIFO="0F" SOCK="0F" OTHER="02"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"

# Migration pass/pash
#read -r PASH_KEYID < "$HOME/.config/password-store/.gpg-id"
PASH_KEYID=530844D43E6ABD5C80B67AD156788BD072D2988E
export PASH_DIR=/media/Datos/linux/repo.git/Pass-ssh-station/password-store}
export PASH_KEYID


# Use arrows to manage history

set -o emacs

alias __A=`echo "\020"`     # up arrow = ^p = back a command
alias __B=`echo "\016"`     # down arrow = ^n = down a command
alias __C=`echo "\006"`     # right arrow = ^f = forward a character
alias __D=`echo "\002"`     # left arrow = ^b = back a character
alias __H=`echo "\001"`     # home = ^a = start of line
alias __Y=`echo "\005"`     # end = ^e = end of line


# Auto startx

if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
	exec startx "$HOME"/.config/X11/xinitrc
fi

}}}
