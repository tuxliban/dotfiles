
= *Media-Editor* =

_Visone script to edit media files_


== Description ==

This script offers a menu with defferent options about media files.

You can convert formats, edit video tracks, add or export subtituls tracks, etc.


== Dependencies ==

* mkvtoolnix-cli
* ffmpeg
* codecs audio/video


== Code: ==


{{{


#!/bin/bash



# Convert and edit media files, formats, codecs, tracks, etc
#
# Dependencies :
#
#	1.- Ffmpeg
#	2.- Mkvtoolnix
#	3.- Codecs audio/video
#





choosefunc() {
option="1.- Convert Video Format\n2.- Add Subtitles Track\n3.- Change Tracks Names\n4.- All The Above\n5.- Extract Subtitles Track\n6.- Trimm Or Merge Videos\n7.- Exit"
echo -e "$option"
read choice
case "$choice" in
	1)funcconvert ;;
	2)funcsubs ;;
	3)funcnames ;;
	4)funcall ;;
	5)funcextsubs ;;
	6)functrim ;;
	7)exit ;;
esac
	
}

funcconvert() {

echo " This Option Will Convert All Videos Match With Format Choosen En The Directory"
echo ""
echo " Choose Video Format Input"
echo ""
echo " MP4  MKV  FLV  WEBM"
read inp
echo ""
echo "Choose Video Format Output"
echo "" 
echo " MP4  MKV  FLV  WEBM"
read out

for i in *.$inp ; do
	mkvmerge -o ${i/$inp}$out $i
	echo "   Operation Complete  "
done

}

funcsubs() {
echo "   Choose Video Format to Add :   " 1 "*.mp4" 2 "*.mkv"
read varvideo
echo "   Choose Subtitles Format to Add:  " 1 "*.srt"  2 "*.ass"
read varsubs
if [ $varvideo = "1" ]
    then
        mp4func
elif [ $varvideo = "2" ]
    then
        mkvfunc
fi
}


mp4func() {
for i in *.mp4 ; do 
    if [ $varsubs = "1" ]
	then
        mkvmerge -o ${i/.mp4}.mkv $i ${i/.mp4}.srt
        echo "   Operation Complete  "
        
    elif [ $varsubs = "2" ]
	then
        mkvmerge -o ${i/.mp4}.mkv $i ${i/.mp4}.ass
        echo "   Operation Complete   "
        
    fi
done
}

        
mkvfunc() {
for i in *.mkv ; do 
    if [ $varsubs = "1" ]
	then
        mkvmerge -o ${i/.mkv}.subs.mkv $i ${i/.mkv}.srt
        echo "   Operation Complete   "
    elif [ $varsubs = "2" ]
	then
        mkvmerge -o ${i/.mkv}.subs.mkv $i ${i/.mkv}.ass
        echo "   Operation Complete   "
    fi
done
}


funcnames() {
echo "Choose name for a title"
read title
echo  "Choose name for audio track"
read audio
echo "Choose name for subtitle track"
read subs
for i in *.mkv ; do
    mkvpropedit $i --edit info --set title=$title --edit track:a1 --set name=$audio --edit track:s1 --set name=$subs --tags all:
done
}

funcall() {
funcconvert
funcnames
}

funcextsubs() {
echo "Choose Subtitles Track to Extract"
read vartrack
echo "Choose Subtitles Format :" 1 "*.srt" 2 "*.ass"
read varsubs

if [ $varsubs = "1" ]
	then
		for i in *.mkv ; do
	mkvextract $i tracks $vartrack:${i/.mkv}.srt 
	done
	elif [ $varsubs = "2" ]
	then
        for i in *.mkv ; do
	mkvextract $i tracks $vartrack:${i/.mkv}.ass
	done
fi
echo "Extraction done"
}

functrim(){
	option="Video Cutter\nVideo Merger"
	echo -e "$option"
	case "option" in
		"Video Cutter") funccut ;;
		"Video Merger") funcmerge ;;
	esac
}


funccut() {

		echo "This Option Will Cut A Portion Of The Video Using START TIME And CUT TIME From A Given Video" 
		echo ""
		echo " Choose Video File"
		echo ""
		ls
		echo ""
		read i
		echo ""
		echo "Choose Video Format Output"
		echo "" 
		echo " MP4  MKV  FLV  WEBM"
		read out
		echo "Choose time to start the cut ---> format hh:mm:ss"
		read ts	
		echo "Choose time to cut ---> format hh:mm:ss"
		read te
		ffmpeg -ss ${ts}.0 -i $i -t ${te}.0 -map 0 -c copy  ${i%.*}.$out
		echo "   Operation Complete  "
}		

funcmerge() {
	echo "This Option Will Merge All Videos In The Directory With The chosen format " 
	echo ""
	echo "Choose Video Format Input"
	echo ""
	echo " MP4  MKV  FLV WEBM "
	echo ""
	read inp
	echo ""
	echo "Choose Video Format Output"
	echo ""
	echo " MP4  MKV  FLV WEBM "
	read out
	i=$(ls *.$inp)
	mkvmerge -o ${i%.*}.$out $i
	echo "   Operation Complete  "
}

choosefunc




}}}

