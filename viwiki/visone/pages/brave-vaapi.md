
= *Brave* =


== Enable Hardware acceleration ==


==== Enter configuration page ====
	
	`brave://flags`


==== Enable flags ====    

	* `#ignore-gpu-blocklist`
	* `#enable-gpu-rasterization`
	* `#enable-zero-copy`
	* `#enable-accelerated-video-decode`
