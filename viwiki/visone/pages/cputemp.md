
= *Dwmblocks CpuTemp Script* =	


== Description ==

This scripts allows you to show your cpu temp in dwmblock bar.

The icon can be set in dwmblock config
	
Edit the path according to your cpu


== Code: ==

{{{

#!/bin/bash
temp=$(cat /sys/class/hwmon/hwmon1/temp1_input | cut -c 1-2)
echo "└ "$temp"ºC ┘"

}}}
