
= *FFMPEG Commands* =


==Ffmpeg transcode software hevc==

_*This options are for a cpu trascode*_



===Options===


==== Video Format ====

	`-pix_fmt yuv420p10le`  --> `10 bits`

==== Video Codec ====
	
	`-c:v libx265`

==== Video Bitrate / max bitrate ====
	
	`-b:v 600k -maxrate 800k`

==== Preset ====
	
	`-preset superfast`

==== Audio Codec ====
	
	`-c:a aac`

==== Audio Format ====
	
	`-af 'volume=2'`

==== Audio Bitrate ====
	
	`-b:a 160k`

==== Varios options ====

	 `-y` --> `overwrite file if exist`



===Example===
		
* _*Transcode a file with a video codec hevc 10bit, video bitrate  600k, audio codec aac, audio bitrate 160k, volume normalize and x2, preset superfast*_ 
		
`ffmpeg -i input-video -c:v libx265 -b:v 600k -pix_fmt yuv420p10le -preset superfast -af "volume=2" -c:a aac -b:a 160k -y output-video`
			

