
= *Aliases Zsh* =

_Visone Aliases for zsh_


== Code: ==


{{{


######## Alias Visone Station ##########

# alias abe-plex
alias plex='rsync -aHAXxv --numeric-ids --progress -e "ssh abe-plex"'

# alias wget 
alias wget='wget --hsts-file="$XDG_DATA_HOME/wget-hsts'

# alias mkdir
alias mkd='mkdir -pv'
alias smkd='doas mkdir -pv'

# alias venom scratchpkg
alias veup='doas scratch sync && doas scratch sysup && doas scratch cache && doas scratch missingdep && doas scratch integrity && doas scratch remove $(scratch orphan) && doas revdep -r'
alias vup='for i in sync sysup cache missingdep integrity "revdep -r" ; do doas scratch $i ; done'
alias scup='doas scratch sync && doas scratch sysup'
alias scpp='doas scratch upgrade'
alias scins='doas scratch install'
alias scser='scratch search'
alias scif='scratch info'
alias scdp='scratch depends'
alias scdd='scratch dependent'
alias scdel='ds scratch remove'
alias scc='scratch cache'
alias sct='scratch cat'
alias scid='scratch installed | wc -l > /tmp/installed'
alias revdep='ds revdep -rye firefox-bin'
alias spkb='doas pkgbuild'
alias spkc='doas pkgbuild --clean'
alias spkd='doas pkgbuild -o'
alias spke='doas pkgbuild -x'

## alias scratch scripts
alias scv='scratch-dev'
alias outv='venom-outdate -v'
alias outc='venom-outdate -c'

# Alias xbps

alias xbl='xbps-query -m | column'
alias xbup='doas xbps-install -Syu'
alias xbins='doas xbps-install -Su'
alias xbdel='doas xbps-remove -RcOon'
alias xbser='xbps-query -Rs'
alias xbinf='xbps-query -R'
alias xbd='xbps-query -Rx'
alias xbdi='xbps-query -RX'
alias xbclean='doas xbps-remove -oO && doas rm -r /var/cache/xbps/*'
alias xbrc='doas xbps-reconfigure -f'
alias xbh='doas xbps-pkgdb -m hold'
alias xbuh='doas xbps-pkgdb -m unhold'


# alias xbps-src
alias srcins='ds xbps-install -f --repository=./hostdir/binpkgs/ '
alias gsrc='./xbps-src'
alias msrcs='./xbps-src -fo static'
alias msrc='./xbps-src -f'
alias gsrc='./xbps-src -a x86_64 -f'
alias srcser='ls /media/Datos/linux/repo.git/void-packages/srcpkgs | grep'
alias srclean='cd /media/Datos/linux/repo.git/void-packages/ && ./xbps-src clean-repocache && ./xbps-src clean && ./xbps-src remove-autodeps && rm -rf hostdir/sources/*'
#alias srclean='cd /media/Datos/linux/repo.git/void-packages/ && rm -rf masterdir-x86_64-musl/builddir/* && rm -rf masterdir-x86_64-musl/destdir/* && rm -rf hostdir/repocache-x86_64-musl/* && rm -rf hostdir/sources/*'
alias rclean='xbps-rindex -r /media/Datos/linux/repo.git/void-packages/hostdir/binpkgs && xbps-rindex -c /media/Datos/linux/repo.git/void-packages/hostdir/binpkgs'
alias srcup='./xbps-src bootstrap-update'
alias reposync='vcp /media/Datos/linux/repo.git/void-packages/hostdir/binpkgs/ /media/Datos/linux/repo.git/visone-void-pkgs/packages/'


# alias fetch & merge venom repo
alias ve-merge='git fetch venom 3.0 && git merge --ff-only venom/3.0 origin/3.0'


# alias DPMS
alias scoff='xset dpms force off'
alias noscoff='xset s off'


# alias su
alias su='doas su'


# alias bat
alias bat='bat.static'

# alias fzf scripts
alias vi='vi-fzf'
alias ff='find . -type f | fzf'
alias yt='vi-fyt'
alias vt='vyts -v'
alias ffl='fzfeed -l'

# alias cat pahe-links
alias cap='cat ~/.config/aria2/links/pahe-links'
alias vap='vim ~/.config/aria2/links/pahe-links'

# alias calenadr remind
alias rem='herbe "$(remind ~/.remind)"'
alias rec='remind -mclvw5,0,0 ~/.remind'

#alias add torrent to aria-links
#alias adt='print "$(xclip -o -sel clip)" >> /tmp/aria-links'
alias adt='print "$(xsel -ob)" >> /tmp/aria-links'
#alias pl='xclip -i -sel clip '
alias pl='xsel -ob'
#alias cli='printf "$(xclip -o -sel clip)" >> ' 
alias cli='printf "$(xsel -ob)" >> ' 

#  Alias Sys monitor
alias sys='watch -n5 system && clear'

# alias patch
alias pt='patch -p1 <'

# alias time
alias tm='herbe "Time: 	$(date +%H:%M)" '

# alias chetsheet linux
alias ct='cht'

# alias passmenu2
alias pss='passmenu2'


# Alias vitorrent aria2 script
alias vtd='vitorrent -d'
alias vto='vitorrent -o'

# alias mpv iptv
alias iptv='mpv --script-opts=iptv=1'
#alias ytpv='devour mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no $(xclip -o -sel clip) && clear'
alias ytpv='devour mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no $(xsel -ob) && clear'

# alias sfeed update
alias sf='sfeed_update ~/.config/sfeed/sfeedrc && clear'

# wiki ftp 
alias vwf='ftp -i visone-wiki@files.000webhost.com'

# pulseaudio start qtbrowser
alias pa='pulseaudio --daemonize=no --exit-idle-time=-1 &'

# Alias doas
alias ds='doas'


# alias coreutils
alias .='cd'
alias 2.='cd ../../'
alias 3.='cd ../../../'
alias 4.='cd ../../../../'
alias 5.='cd ../../../../../'
alias 6.='cd ../../../../../../'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias cl='cd && clear'
alias p='cd -'
alias cpr="cp -r"
alias rmr='rm -r'
alias ld='ls --color -h --group-directories-first'
alias la='ls -lah'
alias lp='ls -lsh'
alias sz='du -sh'
alias chm='chmod +x'
alias chmr='chmod -R +x'
alias vcp='rsync -avuh -progress'
alias svcp='doas rsync -avuh -progress'
alias f='cd $(find . -type d | awk "!/.cache/ , !/.mozilla/" | fzf)'
alias fd='cd $(find /media/Datos/ -type d | fzf)'
alias fl='cd $(find /media/Datos/linux/ -type d -maxdepth 2 | fzf)'
alias fr='cd $(find /media/Datos/linux/repo.git/ -type d -maxdepth 2 | fzf)'

# Alias search
alias as='alias-search'

# Alias translate-cli

alias tne='translator -e'
alias ten='translator -n'


# Visone Wiki
alias vw='vim ~/viwiki/visone/index.md'  

#### alias  services

# alias status runit services
alias svl='ds sv status /var/service/*'

# virt-manager runit alias
alias virtup='doas sv once dbus && doas sv once libvirtd && doas sv once virtlogd && doas sv once virtlockd'
alias virtdown='doas sv down dbus && doas sv down libvirtd && doas sv down virtlogd && doas sv down virtlockd'

# sshd runit  alias
alias shu='doas sv once sshd'
alias shr='ds sv restartd sshd'
alias shd='doas sv down sshd'


# alias zram sysVinit service
alias zramu='doas /etc/rc.d/zram start'
alias zramd='doas /etc/rc.d/zram stop'



###################################


# Alias source shell & alias
alias kr='for i in ~/.mkshrc ~/.aliases; do source $i ; done && clear'
alias br='for i in ~/.bashrc ~/.aliases; do source $i ; done && clear'
alias zr='for i in ~/.zshrc ~/.aliases ; do source $i ; done && clear'

# Alias visone rip ffmpeg/mkvtoolnix

alias sb='series.subs'
alias ads='add.subs'
alias nm='names'
alias rmhevc='rm -i *.S??E??.mkv'
alias rn="rename '.hevc' '' *.mkv"


# Alias update-grub
alias "update-grub"='doas grub-mkconfig -o /boot/grub/grub.cfg'


# Alias visone varios

alias notes='vim /media/Datos/linux/notes.txt'
alias v='vim'
alias sv='doas vim'
alias nf='nerdfetch'
alias uf='ufetch'
alias vf='cd && clear && vifetch'
alias ab="curl -s wttr.in/albacete | awk 'NR==1, NR==17{print}' "
alias font='fc-cache -fv'
alias sib='display ~/wallpapers/BlackAndWhite/*'
alias sx='ls | xargs sxiv -ia "$@" 2>/dev/null'

# Alias Git
alias gc='git clone --depth=1'
alias gb='git branch'
alias gbl='git branch | awk 'NF' '
alias gs='git switch'
alias gu='git add --all && git comit -m "update" && git push origin main'
alias ga='git add'
alias gm='git commit -m'
alias gp='git push origin'
alias gh='git switch 3.0'


# Alias visone change directories

alias cvm='cd /media/Datos/linux/repo.git/void-mklive/'
alias sc=' cd ~/scripts/'
alias sch=' cd ~/scripts/home-page'
alias scdm=' cd ~/scripts/dmenu-scripts'
alias scdw=' cd ~/scripts/dwm-scripts'
alias cc=' cd ~/.config/'
alias ccc=' cd ~/.config/configs-root/'
alias cm='cd ~/.config/mpv/'
alias cnv='cd ~/.config/vim/'
alias cvp='cd /media/Datos/linux/repo.git/void-packages/'
alias cri='cd /media/Datos/linux/repo.git/arch-installer/'
alias crk='cd /media/Datos/linux/repo.git/suckless-builds/'
alias crd='cd /media/Datos/linux/repo.git/dotfiles/'
alias crv='cd /media/Datos/linux/repo.git/forked-repos/venomlinux-visone-ports/'
alias crvv='cd /media/Datos/linux/repo.git/visone-venom-ports/'
alias crr='cd /media/Datos/linux/repo.git/'
alias cdv='cd /media/Datos/Varios/'
alias cdt='cd /media/Datos/TV-Shows/'
alias cdd='cd /media/Datos/Downloads/'
alias cdt='cd /media/Datos/Downloads/Torrenting/'
alias cdl='cd /media/Datos/linux/'
alias ven='cd /media/Datos/linux/venom/'
alias img='cd ~/wallpapers/'
alias imgl='cd ~/wallpapers/Landscape/'
alias clp='cd /media/Datos.Local/'
alias cvi='cd ~/.config/configs-root/visone-void-installer/'
alias cpl='cd /usr/ports/local/'
alias cpdl='cd /usr/ports/dev-local/'
alias cpm='cd /usr/ports/main/'


# Alias Edit Configs Vim

alias vz='vim ~/.zshrc'
alias vb='vim ~/.bashrc'
alias vk='vim ~/.mkshrc'
alias va='vim ~/.aliases'
alias vp='vim ~/.profile'
alias vr='vim ~/.config/qutebrowser/config.py'
alias vsp='vim ~/.config/mini-startpage/index.html'
alias vnv='vim ~/.config/vim/init.vim'
alias vv='vim ~/.vim/vimrc'
alias vrp='vim ~/.config/qutebrowser/repos'
alias vsw='vim ~/.config/spectrwm/spectrwm.conf'
alias vmux='vim ~/.config/tmux/tmux.conf'




  }}}
