
= *Qutebrowser* =


== Enable Hardware acceleration ==

*Add this to config.py*

*Code*

{{{

c.qt.args = ["enable-native-gpu-memory-buffers", "ignore-gpu-blocklist", "enable-gpu-rasterization", "num-raster-threads=8", "enable-accelerated-video-decode", "use-gl=desktop"]

}}}
