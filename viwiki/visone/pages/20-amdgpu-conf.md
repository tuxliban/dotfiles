
= *20-AMDGPU-Conf* =

 _Visone  GPU Config_

 _AMD RX 550_


=== Code: ===

 {{{

Section "Device"
	Identifier "AMD"
	Driver     "amdgpu"
	Option "Tear Free" "true"
	Option "DRI" "3"
	Option "VariableRefresh" "true"
EndSection

}}}
