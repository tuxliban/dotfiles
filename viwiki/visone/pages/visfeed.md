
= *Visfeed* =

*Sfeed script to manages RSS feeds*

== *Dependencies* ==

* Sfeed
* Dmenu
* Mpv
* Web-browser


== *Code* ==

{{{


#!/bin/sh

# mpv options cache
# --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M


dm="dmenu -b -X 370 -Y 20 -W 1200 -l 2 -g 1"


void() {
# url void-packages feed
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/Q Void-Packages" | grep '^N' | $dm -p "New Void-Packages Updated: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
test -n "${url}" && qutebrowser "${url}"
}

linux() {
# url youtube linux feed
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/"M* | grep '^N' | $dm -p "New Youtube Linux Feeds: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
test -n "${url}" && mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no -ytdl-format="bv*[height=720][ext=mp4]+ba/b" "${url}" 
}

reggae_new() {
# url youtube reggae feed new
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/"R* | grep '^N' | $dm -p "New Youtube Reegae Feeds: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
test -n "${url}" && mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no -ytdl-format="bv*[height=720][ext=mp4]+ba/b" "${url}" 
}

reggae_history() {
# url youtube regge feed history
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/"R* | $dm -p "Reggae Feeds History: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
test -n "${url}" && mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no -ytdl-format="bv*[height=720][ext=mp4]+ba/b" "${url}" 
}

web() {
# url web feed
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/"Q* | grep '^N' | $dm -p "New Web Feeds: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
test -n "${url}" && qutebrowser "${url}"
}

search() {
# url feed search
url=$(sfeed_plain "/media/Datos/linux/sfeed/feeds/"* | grep '^N' | $dm -p "Feed Search: "| sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
		
if [ $(printf "$url" | grep youtube | wc -l) -ge 1 ]
then
test -n "${url}" && mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no -ytdl-format="bv*[height=720][ext=mp4]+ba/b" "${url}" 
else
test -n "${url}" && qutebrowser "${url}"
fi
}

help() {

printf "Usage:\nvisfeed -v for Void-Packages feed\nvisfeed -l for YT Linux feeds\nvisfeed -rn for YT New Reggae feeds\nvisfeed -r for YT Reggae History feeds\nvisfeed -w for Web feeds\nvisfeed -s 'string' for Search Feeds by String\n "

}


case "$1" in
	-v) 	void	;;

	-l)	linux	;;
	
	-r)	reggae_history	;;

	-rn)	reggae_new	;;

	-w)	web	;;
	
	-s)	search	;;

	*)	help	;;	

	
esac

  }}}
