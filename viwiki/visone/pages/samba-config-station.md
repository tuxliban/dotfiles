
= *Samba Config Station* =

 _Visone Samba Config_
 
  _Station_	


=== Code: ===

{{{
#===== Global Settings =====

[global]

# workgroup = Workgroup-Name, eg: MIDEARTH
   workgroup = MYGROUP
   server string = Samba Server
   server role = standalone server

;   hosts allow = 192.168.1. 192.168.2. 127.
;  guest account = pcguest

   log file = /usr/local/samba/var/log.%m
   max log size = 50

;   interfaces = 192.168.12.2/24 192.168.13.2/24 
;   wins support = yes


# Share dirs

[station]
	path = /media/Datos/
	valid users = visone
	writable = yes
	browseable = yes

}}}
