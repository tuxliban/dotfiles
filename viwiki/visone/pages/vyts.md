
= *Vyts* =

*Youtube videos searcher*

== *Dependencies* ==

* Yt-dlp or any fork
* Mpv
* Dmenu

== *Code* ==

{{{


#! /bin/sh


dm="dmenu -b -X 370 -Y 20 -W 1200 -l 5 -g 1"

# individious servers db are in https://redirect.invidious.io/

# Servers
#INVIDIOUS_URL_BASE='https://invidious.kavin.rocks'
#INVIDIOUS_URL_BASE='https://invidious.snopyta.org'
INVIDIOUS_URL_BASE='https://vid.puffyan.us'

FIFO_FILE=~/.config/mpv/mpv.fifo
FOLDER_TEMP=/tmp/yt
TMP_HTML="$FOLDER_TEMP/yt_temp.html"
DATA_NAMES="$FOLDER_TEMP/names"
DATA_CHANNEL="$FOLDER_TEMP/channel"
DATA_OLD="$FOLDER_TEMP/old"
DATA_TIME="$FOLDER_TEMP/time"
DATA_ID="$FOLDER_TEMP/id"
FULL_DATA="$FOLDER_TEMP/fulldata"

getDataFromInvidious() {
	selection=$( echo $1 | sed 's/ /+/g' )
	curl -L "$INVIDIOUS_URL_BASE/search?q=$selection" -o "$TMP_HTML"
}


selection="$1"
[ "$selection" = "" ] && exit 0

[ ! -d "$FOLDER_TEMP" ] && mkdir -p $FOLDER_TEMP
getDataFromInvidious "$selection"


# 
# process data
#

# name of video
cat $TMP_HTML | grep auto | grep -v channel | grep -v video | awk -F'>' {'print $2'} | sed 's/<\/p//g' > $DATA_NAMES
# video ID
cat $TMP_HTML | grep watch | grep -v title | grep  style | awk -F'v=' {'print $2'} | awk -F'\"' {'print $1'} > $DATA_ID
# name of the channel
cat $TMP_HTML | grep channel | grep -v flex | grep -v page |awk -F'>' {'print $2'} | sed 's/<\/p//g' > $DATA_CHANNEL
# time duration
cat $TMP_HTML | grep length | awk -F'>' {'print $2'} | sed 's/<\/p//g' > $DATA_TIME
# how old is the video
cat $TMP_HTML | grep Shared | awk -F'>' {'print $2'} | sed 's/Shared //' | sed 's/<\/p//g' > $DATA_OLD

readarray -t arrayI < $DATA_ID
readarray -t arrayN < $DATA_NAMES
readarray -t arrayC < $DATA_CHANNEL
readarray -t arrayT < $DATA_TIME
readarray -t arrayO < $DATA_OLD
 
[ -f $FULL_DATA ] && rm $FULL_DATA

for index in "${!arrayI[@]}"; do
	echo "${arrayI[$index]} >> ${arrayN[$index]} >> ${arrayC[$index]} >> ${arrayT[$index]} >> ${arrayO[$index]}" >> $FULL_DATA
done


# choose episode with dmenu

selection=$( cat $FULL_DATA | awk -F ' >> ' '{print "[" $4 "]  " $2 "  <<" $3 ">>  " $5}' | $dm -p "Choose Video: ")
[ -z "$selection" ] && exit 0
filter=$( echo $selection | awk -F'] ' '{print $2}' | awk -F' <<' '{print $1}' )
selected=$( cat $FULL_DATA | grep "$filter" | awk -F' >> ' '{print $1}' )
[ -z "$selected" ] && exit 0

# mpv options to choose 720p with x264 codec

mpv --cache=yes --demuxer-max-bytes=500M --demuxer-max-back-bytes=100M --save-position-on-quit=no -ytdl-format="bv*[height=720][ext=mp4]+ba/b" "https://www.youtube.com/watch?v="$selected""

  }}}
