
= *Pulsemixer* =


	* This script is a terminal alternative 
	  to manage pulseaudio or pipewire server
	  
	  [[https://github.com/GeorgeFilipkin/pulsemixer|Pulsemixer Git Repo]]


== Options ==


_*Via context menu it is possible to set different options*_:


=== set-default-sink === 

*  Configure any sink as default sink

=== set-default-source === 

* Configure any source as default source

=== move-sink-input === 

*  Move the specified playback stream (identified by its numerical index) to the specified sink (identified by its symbolic name or numerical index)

=== move-source-output === 

*  Move the specified recording stream (identified by its numerical index) to the specified source (identified by its symbolic name or numerical index)

=== suspend-sink === 

*  Suspend or resume the specified sink (which my be specified either by its symbolic name, or by its numeric index), depending whether 1 (suspend) or 0 (resume) is passed as last argument. Suspending a sink will pause all playback. Depending on the module implementing the sink this might have the effect that the underlying device is closed, making it available for other applications to use. The exact behaviour depends on the module

=== suspend-source === 

*  Suspend or resume the specified source (which my be specified either by its symbolic name, or by its numeric index), depending whether 1 (suspend) or 0 (resume) is passed as last argument. Suspending a source will pause all capturing. Depending on the module implementing the source this might have the effect that the underlying device is closed, making it available for other applications to use. The exact behaviour depends on the module

=== set-sink-port === 

*  Assing specific port to a sink

=== set-source-port === 

*  Assing specific port to a source

=== kill-client === 

=== kill-sink-input === 

=== kill-source-output === 

=== set-card-profile === 


_See man pactl for details on these features_
