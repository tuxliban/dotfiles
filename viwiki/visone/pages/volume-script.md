
= *Dwm-vol Script* =

 _Visone dwm-vol Script_
 
== Dependencies: ==

		* pulseaudio or pipewire
		* libnotify

== Description ==

*  This script allows you to manage the volume.
*  It can be keybinded in any DE/WM
*  Any option will show you a notifycation 


== Usage ==

		* Volume Up 5%
			
			`dwm-vol +`
			
		* Volume Down 5%
			
			`dwm-vol -`
			
		* Volume toggle mute
		
			`dwm-vol m`


== Code: ==


{{{

#!/bin/bash

case $1 in
	+)pactl set-sink-volume @DEFAULT_SINK@ +5%
	pkill -RTMIN+10 dwmblocks
	notify-send "Volume Up 5%" "VOL $(pulsemixer --get-volume | awk '{print $1}')%" ;;

	-)pactl set-sink-volume @DEFAULT_SINK@ -5%
	pkill -RTMIN+10 dwmblocks
	notify-send "Volume Down 5%" "VOL $(pulsemixer --get-volume | awk '{print $1}')%" ;;

	m)pactl set-sink-mute @DEFAULT_SINK@ toggle
	  pkill -RTMIN+10 dwmblocks
	  notify-send " Volume Toggle" "Vol $(pulsemixer --get-volume | awk '{print $1}')%)" ;;

esac

}}}


= Update =

_*This new version it can choose between audio sinks when you have more than one running*_


== Dependencies: ==

	* Herbe (lightweigh notify applicacion daemonless)
	* Pulseaudio or Pipewire
	* Pulsemixer (check the wiki for info)
	
	
== Code: ==


{{{


#!/bin/bash

plus() {

	sink=$(pulsemixer --list-sinks | awk /sink-input/'{gsub(/,/, ""); print $6 "  " $4}')
	if [ $(echo "$sink" | wc -l) -eq 1 ]
	then
		pu=$(pulsemixer --change-volume +10 --id $(echo "$sink" | awk '{print $2}') --get-volume --id $(echo "$sink" | awk '{print $2}'))	
		herbe "Volume UP 10% to" "$(echo "$pu")" 
	else
		op=$(echo "$sink" | rofi -dmenu) 
		pu=$(pulsemixer --change-volume +10 --id $( echo "$op" | awk '{print $2}') --get-volume --id $(echo "$op" | awk '{print $2}' ))
		herbe "Volume UP 10% to" "$(echo "$pu")" 
	fi

}

minus() {

	sink=$(pulsemixer --list-sinks | awk /sink-input/'{gsub(/,/, ""); print $6 "  " $4}')
	if [ $(echo "$sink" | wc -l) -eq 1 ]
	then
		pu=$(pulsemixer --change-volume -10 --id $(echo "$sink" | awk '{print $2}') --get-volume --id $(echo "$sink" | awk '{print $2}'))	
		herbe "Volume Down 10% to" "$(echo "$pu")" 
	else
		op=$(echo "$sink" | rofi -dmenu) 
		pu=$(pulsemixer --change-volume -10 --id $( echo "$op" | awk '{print $2}') --get-volume --id $(echo "$op" | awk '{print $2}' ))
		herbe "Volume Down 10% to" "$(echo "$pu")" 
	fi


}

mute() {

	sink=$(pulsemixer --list-sinks | awk /sink-input/'{gsub(/,/, ""); print $6 "  " $4}')
	if [ $(echo "$sink" | wc -l) -eq 1 ]
	then
		pu=$(pulsemixer --toggle-mute --id $(echo "$sink" | awk '{print $2}'))
		herbe "Volume Mute" "$( echo "$pu")"
	else
		op=$(echo "$sink" | rofi -dmenu)
		pu=(pulsemixer --toggle-mute --id $( echo "$op" | awk '{print $2}'))	
		herbe "Volume Mute $(echo "$pu")"
	fi


}


case "$1" in

	+) plus ;;
	-) minus ;;
	m) mute ;;
esac
  }}}
