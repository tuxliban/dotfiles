
= *Dunst Config* =

 _Visone Dunst Config_


=== Code: ===

{{{

[global]
 ### Display ###

monitor = 0
follow = mouse

# Geometry
geometry = "300x30-30+30"
indicate_hidden = yes
# width is 0.
shrink = no

# Transparency    
transparency = 40

# Notification  height 
notification_height = 0

# Separator    
separator_height = 0

# Padding between text and separator.
padding = 8

# Horizontal padding.
horizontal_padding = 8

# Defines width in pixels of frame around the notification window.
# Set to 0 to disable.
frame_width = 3

# Defines color of the frame around the notification window.
frame_color = "#45733b"
separator_color = frame

# Sort messages by urgency.
sort = yes

 # Don't remove messages, if the user is idle (no mouse or keyboard input)
# for longer than idle_threshold seconds.
# Set to 0 to disable.
# A client can set the 'transient' hint to bypass this. See the rules
# section for how to disable this if necessary
idle_threshold = 10

### Text ###

font = Cantarell 10
line_height = 0

markup = full
format = "<b><u>%s</u></b>\n%b"
alignment = center
vertical_alignment = center
show_age_threshold = 30
word_wrap = yes
ellipsize = middle
ignore_newline = no
stack_duplicates = true
hide_duplicate_count = false
show_indicators = yes

### Icons ###

icon_position = left
min_icon_size = 0
max_icon_size = 32
icon_path = /usr/share/icons/hicolor/24x24/status/:/usr/share/icons/hicolor/24x24/devices/:/usr/share/icons/hicolor/24x24/apps/

### History ###

sticky_history = yes
history_length = 20

### Misc/Advanced ###

dmenu = /usr/bin/dmenu -p dunst:
browser = /usr/bin/firefox -new-tab
always_run_script = true
title = Dunst
class = Dunst
startup_notification = false
verbosity = mesg
corner_radius = 25
ignore_dbusclose = false

### Legacy

force_xinerama = false

### mouse

mouse_left_click = close_current
mouse_middle_click = do_action, close_current
mouse_right_click = close_all

[experimental]
per_monitor_dpi = false

[shortcuts]

    close = ctrl+space
    close_all = ctrl+shift+space

   # history = ctrl+grave

    context = ctrl+shift+period

[urgency_low]
    background = "#4a3f36"
    foreground = "#f2e9e9"
    timeout = 3
    icon = /home/visone/.config/dunst/bell.jpg

[urgency_normal]
    background = "#4a3f36"
    foreground = "#f2e9e9"
    timeout = 3
    icon = /home/visone/.config//dunst/bell.jpg

[urgency_critical]
    background = "#4a3f36"
    foreground = "#f2e9e9"
    frame_color = "#ad0309"
    timeout = 5
    icon = /home/visone/.config/dunst/alert.jpg


#[transient_disable]
#    match_transient = yes
#    set_transient = no

#[transient_history_ignore]
#    match_transient = yes
#    history_ignore = yes

#[fullscreen_delay_everything]
#    fullscreen = delay
#[fullscreen_show_critical]
#    msg_urgency = critical
#    fullscreen = show

#[espeak]
#    summary = "*"
#    script = dunst_espeak.sh

#[script-test]
#    summary = "*script*"
#    script = dunst_test.sh

#[stack-volumes]
#    appname = "some_volume_notifiers"
#    set_stack_tag = "volume"
#
# vim: ft=cfg
