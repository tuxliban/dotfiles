
= *Vifetch-void* =

*Visone fetch 4 void-linux script*

== *Code* ==

{{{


#!/bin/sh


kernel="$(uname -r)"
os="$(awk -F'"' '/PRETTY/ {print toupper($2)}' /etc/os-release)"
shell=$(printf "$(basename $SHELL)" | awk '{print toupper($0)}')
init=$(awk '{print toupper($0)}' /proc/1/comm)
uptime="$(uptime -p | sed 's/up //')"
cpu="$(lscpu | awk '/Nombre/ {print $4, $5, $6, $7}')"
gpu="$(awk -F'"' '/Chipset/ {print $2}' /var/log/Xorg.0.log)"
tr="$(printf "$TERM" | awk '{print toupper($0)}')"
#font="$(cat "${HOME}/.gtkrc-2.0" | grep font | cut -c 16-30 | awk '{print toupper($0)}')"
font="$(awk -F'"' '/font/ {print toupper($2)}' "${HOME}/.gtkrc-2.0")"
wm="$(awk '/dwm/ {print toupper($2)}' "${HOME}/.xinitrc")"
pkg="$(xbps-query -l | wc -l)"
mem=$(free -m | awk '/^Mem:/ {print $3 "MiB / " $2 "MiB"}')
st=$(df -h / | awk '/dev/{print $3" / "$2}')
file="$(printf "$FILE" | awk '{print toupper($0)}')"
br="$(printf "$BROWSER" | awk '{print toupper($0)}')"
ed="$(printf "$EDITOR" | awk '{print toupper($0)}')"
iv="$(printf "$IMAGEVIEWER" | awk '{print toupper($0)}')"
mn="$(printf "$MENU" | awk '{print toupper($0)}')"

## DEFINE COLORS

bold='[1m'
black='[30m'
red='[31m'
green='[32m'
yellow='[33m'
blue='[34m'
magenta='[35m'
cyan='[36m'
white='[37m'
grey='[90m'
reset='[0m'


## USER VARIABLES -- YOU CAN CHANGE THESE

lc="${reset}${bold}${magenta}"  
nc="${reset}${bold}${yellow}"   
hn="${reset}${bold}${blue}"     
ic="${reset}${green}"           
c0="${reset}${grey}"            
c1="${reset}${white}"           
c2="${reset}${yellow}"          


full() {
## Void-Linux
cat <<EOF
╭───────────────────────────────────────────────╮

	${lc}HW -> ${ic}Visone-Station${reset}		
	${lc}|- Cpu -> ${ic}${cpu}${reset}	
	${lc}|- Gpu -> ${ic}${gpu}${reset}	
	${lc}|- Ram -> ${ic}${mem}${reset}
	${lc}|- /Root -> ${ic}${st}${reset} 
							
	${lc}OS -> ${ic}${os}-Musl${reset}	
	${lc}|- Uptime -> ${ic}${uptime}${reset} 
	${lc}|- Kernel -> ${ic}${kernel}
	${lc}|- Init -> ${ic}${init}${reset}
	${lc}|- Pkgs -> ${ic}${pkg}${reset}
	${lc}|- Shell -> ${ic}${shell}${reset} 
							
	${lc}WM-> ${ic}${wm}${reset}
	${lc}|- Menu -> ${ic}${mn}${reset}
	${lc}|- Browser -> ${ic}${br}${reset}
	${lc}|- Files -> ${ic}${file}${reset}
	${lc}|- Terminal -> ${ic}${tr}${reset}
	${lc}|- Editor -> ${ic}${ed}${reset}
	${lc}|- ImageViewer -> ${ic}${iv}${reset}
	${lc}|- Font -> ${ic}${font}${reset}

                                               	
╰───────────────────────────────────────────────╯

EOF
}

hw() {
cat <<EOF
╭───────────────────────────────────────────────╮

	${lc}HW -> ${ic}Visone-Station${reset}		
	${lc}|- Cpu -> ${ic}${cpu}${reset}	
	${lc}|- Gpu -> ${ic}${gpu}${reset}	
	${lc}|- Ram -> ${ic}${mem}${reset}
	${lc}|- /Root -> ${ic}${st}${reset} 

╰───────────────────────────────────────────────╯

EOF
}

sys() {

cat <<EOF
╭───────────────────────────────────────────────╮

	${lc}OS -> ${ic}${os}-Musl${reset}	
	${lc}|- Uptime -> ${ic}${uptime}${reset} 
	${lc}|- Kernel -> ${ic}${kernel}
	${lc}|- Init -> ${ic}${init}${reset}
	${lc}|- Pkgs -> ${ic}${pkg}${reset}
	${lc}|- Shell -> ${ic}${shell}${reset} 

╰───────────────────────────────────────────────╯

EOF
}


sf() {

cat <<EOF
╭───────────────────────────────────────────────╮

	${lc}WM-> ${ic}${wm}${reset}
	${lc}|- Menu -> ${ic}${mn}${reset}
	${lc}|- Browser -> ${ic}${br}${reset}
	${lc}|- Files -> ${ic}${file}${reset}
	${lc}|- Terminal -> ${ic}${tr}${reset}
	${lc}|- Editor -> ${ic}${ed}${reset}
	${lc}|- ImageViewer -> ${ic}${iv}${reset}
	${lc}|- Font -> ${ic}${font}${reset}

╰───────────────────────────────────────────────╯
EOF
}


case $1 in

	-f) full ;;
	-y) sys ;;
	-h) hw ;;
	-s) sf ;;
esac

  }}}
