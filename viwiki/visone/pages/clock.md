
=*Dwmblocks Clock Script*=

 _Visone Clock Script_

== Description ==

This script allows you to show the date and time in your dwmblock bar

The icon can b set in dwmblock config

== Options ==

*  Change the date time format 


== Code: ==

{{{

#!/bin/bash

dte="$(date +"%a-%d-%m-%y %H:%M")"
echo -e "└ $dte ┘"

#dte="$(date +"%a, %B %d %l:%M%p"| sed 's/  / /g')"

}}}
