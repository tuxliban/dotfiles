
= *Polybar Configuration* =
 
  _Visone Polybar config for Bspwm_
  
  
== Code: ==  
  
{{{


[global/wm]
margin-top = 1
margin-bottom = 1

[settings]
throttle-output = 5
throttle-output-for = 10
screenchange-reload = true
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground = #050505
format-background = #ffffff
;format-underline =
;format-overline =
format-spacing = #080807
;format-padding =
;format-margin =
;format-offset =


[colors]

background = ${xrdb:color1:#222}
foreground = ${xrdb:color7:#222}
foreground-alt = ${xrdb:color7:#222}
primary = ${xrdb:color1:#222}
secondary = ${xrdb:color2:#222}
alert = ${xrdb:color3:#222}



###############################
###############################
####     MAINBAR-BSPWM     ####
###############################
###############################

[bar/vbspwm]
monitor = HDMI-0
monitor-fallback = HDMI-0
width = 95%
height = 20
offset-x = 2.5%
offset-y = 4
fixed-center = true
bottom = false

 background = ${colors.background}
foreground = ${colors.foreground}


wm-restack = bspwm
override-redirect = true

enable-ipc = true

padding-left = 6
padding-right = 6

module-margin-left = 1
module-margin-right = 1

font-0 = "Noto Sans:size=11;2"
font-1 = "Font Awesome:size=11;2"
font-2 = "Font Awesome 5 Free:style=solid:size=12;2"
font-3 = "Font Awesome 5 Brands:style=solid:size=12;2"
font-4 = "Weather Icons:size=12;2"
font-5 = "Noto Sans Mono:size=11;2"

modules-left = aa temperature1 ac aa fan ac aa cpu1 ac aa memory1 ac aa filesystem ac
modules-center = ba bspwm bc
modules-right = player-moc aa openweathermap-fullfeatured ac aa updates-pacman-aurhelper ac aa pulseaudio ac aa date ac

#tray-detached = false
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 10
tray-maxsize = 80
tray-scale = 1.0
tray-position = right
tray-background = ${colors.background}

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev


######################################
######################################
#####     MAINBAR-BSPWM-EXTRA     ####
######################################
######################################

[bar/vbspwm-extra]
monitor = ${env:MONITOR}
;monitor-fallback = HDMI1
width = 100%
height = 30
;offset-x = 1%
;offset-y = 1%
radius = 0.0
fixed-center = true
bottom = true

background = ${colors.background}
foreground = ${colors.foreground}

wm-restack = bspwm
override-redirect = true

enable-ipc = true

padding-left = 8
padding-right = 8

module-margin-left = 2
module-margin-right = 2

font-0 = "Noto Sans:size=10;2"
font-1 = "Font Awesome:size=11;2"
font-2 = "Font Awesome 5 Free:style=solid:size=12;2"
font-3 = "Font Awesome 5 Brands:style=solid:size=12;2"
font-4 = "Weather Icons:size=12;2"
font-5 = "Noto Sans Mono:size=10;2"

modules-left =  temperature1 fan fan1 cpu1 memory1 filesystem
modules-center = bspwm
modules-right = player-moc  openweathermap-fullfeatured updates-pacman-aurhelper pulseaudio date


tray-detached = false
;tray-offset-x = 0
;tray-offset-y = 0
;tray-padding = 2
tray-maxsize = 20
;tray-scale = 1.0
tray-position = right
tray-background = ${colors.background}

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev


################################
################################
#####     MODULE BSPWM     #####
################################
################################

[module/bspwm]
type = internal/bspwm

enable-click = true
enable-scroll = true
reverse-scroll = true
pin-workspaces = true



ws-icon-0 = 1;I
ws-icon-1 = 2;II
ws-icon-2 = 3;III
ws-icon-3 = 4;IV
ws-icon-4 = 5;V
ws-icon-5 = 6;VI
ws-icon-6 = 7;VII
ws-icon-7 = 8;VIII
ws-icon-8 = 9;IX
ws-icon-9 = 10;X


format =  <label-state>
label-focused = %icon%
label-focused-background = #8803fc
label-focused-padding = 1
label-focused-foreground = ${colors.foreground}
label-occupied = %icon%
label-occupied-padding = 1
label-occupied-background = ${colors.background}
label-occupied-foreground = #03fc39
label-urgent = %icon%
label-urgent-padding = 2
label-empty = %icon%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 2
label-empty-background = ${colors.background}


format-foreground = ${colors.foreground}
format-background = ${colors.background}


#####################################
#####################################
######     Visone---Modules     #####
#####################################
#####################################

[module/cpu1]
type = internal/cpu
interval = 1
format-foreground = ${colors.foreground}
format-background = ${colors.background}
;    
format-prefix =""
format-prefix-foreground = ${colors.foreground}
format = <label> 
label-font = 3
label =%percentage:3%%



####################################

[module/date]
type = internal/date
interval = 60
date = " %a-%d-%m-%y"
date-alt = " %d-%mmm-%yyy"
time = %H:%M
time-alt = %H:%M
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-foreground = ${colors.foreground}
format-background = ${colors.background}
label-active-font = 4
label = %time% %date% 

###################################

[module/filesystem]
type = internal/fs

; Mountpoints to display
mount-0 = /
interval = 600
format-mounted = <label-mounted>
format-mounted-foreground = ${colors.foreground}
format-mounted-background = ${colors.background}
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
label-mounted =   %mountpoint%: %free% 
label-unmounted = %mountpoint% not mounted
format-unmounted-foreground = ${colors.foreground}
format-unmounted-background = ${colors.background}


##################################

[module/memory1]
type = internal/memory
interval = 10
label = %gb_used%
bar-used-indicator =
bar-used-width = 5
bar-used-foreground-0 = ${colors.foreground}
bar-used-fill = 
bar-used-empty = 
bar-used-empty-foreground = #3af03a
format = <label>  
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-foreground = ${colors.foreground}
format-background = ${colors.background}


#################################

[module/temperature1]
type = internal/temperature
interval = 1
hwmon-path = /sys/class/hwmon/hwmon1/temp1_input
warn-temperature = 75
format =  <ramp> <label> 
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-warn = <ramp> <label-warn>
label = %temperature-c%
label-warn =  %temperature-c%
label-warn-foreground = #cd1f3f
ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-3 = 
ramp-4 = 
ramp-foreground = ${colors.foreground}

################################


[module/openweathermap-fullfeatured]
type = custom/script
exec = ~/.config/polybar/scripts/openweathermap-fullfeatured.sh
interval = 600
label-font = 5
format-foreground = ${colors.foreground}
format-background = ${colors.background}


[module/updates-pacman-aurhelper]
type = custom/script
exec = ~/.config/polybar/scripts/updates-pacman-aurhelper.sh
interval = 36000
format-foreground = ${colors.foreground}
format-background = ${colors.background}

###############################

[module/player-moc]
type = custom/script
if_exec = "pgrep mocp"
exec = ~/.config/polybar/scripts/player-moc.sh
interval = 1800
format-prefix = " "
format-foreground = ${colors.foreground}
format-background = ${colors.background}
click-left = mocp --previous &
click-right = mocp --next &
click-middle = mocp -x &

##############################

[module/pulseaudio]
type = internal/pulseaudio
interval = 5
format-volume =  <ramp-volume> <label-volume> 
label-volume-background = ${colors.background}
label-volume-foreground = ${colors.foreground}
ramp-volume-0 =  
;ramp-volume-1 =  
;ramp-volume-2 =  
ramp-volume-background = ${colors.background}
ramp-volume-foreground = ${colors.foreground}
click-right = pavucontrol &

#############################

[module/fan]
type = custom/script
exec = ~/.config/polybar/scripts/fan.sh
interval = 10
label-font = 2
format-foreground = ${colors.foreground}
format-background = ${colors.background}

############################

[module/fan1]
type = custom/script
exec = ~/.config/polybar/scripts/fan1.sh
interval = 10
label-font = 2
format-foreground = ${colors.foreground}
format-background = ${colors.background}

###########################


; Separadores ------------

[module/aa]
type = custom/text
content ="└"
label-padding= 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}


[module/arribaabre2]
type = custom/text
content ="└"
label-padding= 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}


[module/ac]
type = custom/text
content = "┘"
label-padding= 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}


[module/ba]
type = custom/text
content = "┌"
label-padding= 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}


[module/bc]
type = custom/text
content = "┐"
label-padding= 0
content-foreground = ${colors.foreground}
content-background = ${colors.background}


[module/arribacierra2]
type = custom/text
content = "┘"
label-padding= 0
content-foreground = ${colors.color10}
content-background = ${colors.background}

}}}
