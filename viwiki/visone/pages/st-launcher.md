
== *St launcher* ==

_*This scripts funtion as launcher for st-256color terminal assinging position.*_
_*I wrote it to use it in dwm, so adding a wm rule to launch any instance of st
in floating mode, you can set the position of the window.*_

=== *Dependencies: ===

	*  st-256color terminal
	*  vranger file (check the code )
	*  pulsemixer (check wiki)
	
=== Code: ===

{{{

#!/bin/sh

# Usage: 
# st-launcher -s for a terminal
# st-launcher -r for ranger with pywal
# st-launcher -p for pulsemixer
# st-launcher -r for nnn
# vranger:
# (cat ~/.cache/wal/sequences &)
# ranger

case $1 in

	-s) st -g 189x10+10+854 ;;
	-r) st -g 189x20+10+644 -e vranger ;;
	-p) st -g 189x10+10+854 -e pulsemixer ;;
	-n) st -g 189x10+10+854 -e nnn ;;
esac


}}}
