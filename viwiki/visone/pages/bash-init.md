
== *Using Bash as Init* ==

_Using Bash as init is an alternative of sort to using chroot._
_In this manual we explain how to change your passwords using your installed system._

=== *Assuming* ===

*  You have Grub acces   


== *Process* ==

* Boot up our system  

* In the Grub screen enter in the edit mode pulsing `e`    

* Add `init=/bin/bash` in the kernel options line    

* Pulse `Ctrl+x` to run your system with that option    

* Remount the root partition as read/write with `mount -n -o remount,rw /`     


=== *Changing Passwords* ===

* Use the command `users` to see the list of all availabe users in the system     
  
* Change your user password with `passwd <username>` 

_The system will ask you to write your new password two times, the first to set it and the second to confirm it_    
  
* Change Root password with `passwd`

_The process is the same as changing the user password_


== *Reboot* ==

*  Reboot the system with `reboot -f`
