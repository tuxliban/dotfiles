
= *Pulsemixer* =


_*This script is a terminal alternative to manage pulseaudio or pipewire server*_
	  
	  [[https://github.com/GeorgeFilipkin/pulsemixer|Pulsemixer Git Repo]]


== Examples ==


=== list devices === 
      
      `pulsemixer --list`

=== Get volume ===
      
      `pulsemixer --get-volume --> without id = @DEFAILT_SINK@`

=== Change volume ===
      
      `pulsemixer --change-volume {+-}N --> without id == @DEFAULT_SINK@`
      
      `pulsemixer --set-volume X --> without id = @DEFAULT_SINK@`

=== Mute toggle ===
	
	`pulsemixer --toggle-mute --> without id = @DEFAULY_SINK@`
