
= *Change CPU Governor* =

_This scripts allows you to change the gorvernor of your cpu_


== Dependencies: ==

	*  cpufreq



== Code: ==


{{{


#!/bin/sh

# Usage: script-name <option>
# `script-name ondemand`
# to list governor options do
# cat /sys/devices/system/cpu/cpu0/cpufreq/cpu0/scaling_available_governors

printf "$1\n" | tee  /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
printf "Performance bias on all processor cores has been set to $1\n"




}}}
