
= *Nvidia Drivers Manjaro* =

 _*This manual explains the right way to install propietary nvidia drivers*_



== *Installation* ==


=== Check nvidia model support ===


_*Check if your nvidia gpu model is named in this link. if it is it means that your nvidia gpu model is not supported for the new drivers and you will need to use the free drivers.*_
	

[[https://nouveau.freedesktop.org/CodeNames.html|Nouveau Freedestop.org]]


=== It is supported ===



==== To install propietary nvidia drivers use this command ====
    
_*After the recent changes of nvidia drivers and the following version's split between 470xx and 510xx and also de abandoned of the legacy drivers, the nvidia drivers can clasified like this:*_

	*  _*For models newer than serie 800, the recommended driver would be the version 510, that would be vide-nvidia driver*_
	*  _*For models older than serie 700, the reccomend driver would be the version 470xx except 3 models that still have support with the last version(510):*_

		* GTX 750 ti
		* GTX 750
		* GTX 445

	* _*For older models and with kernel 5.15 or newer, it should use nouveau*_



_*For the installation of recommend drivers do*_
    
 `sudo mhwd -a pci nonfree 0300`  Privative drivers 
 `sudo mhwd -a pci free 0300`     Free drivers (nouveau) 



== Dual Gpu ==

_*There is various ways to manage dual-gpu combos depending of your brands/models*_


    		
=== BUMBLEBEE ===


==== Check bumblebee is working ====

`optirun glxgears -info` 

==== Launch applications with nvidia gpu ====

`optirun <options> applicaiton <options>` 

==== Nvidia-Settings use ====

`optirun nvidia-settings -c:8` 

==== Examples ====

`optirun wine app/win.exe` 

`optirun blender` 

==== Config file path ====

`/etc/bumblebee/bumblebee.conf` 

==== More Info ====

[[https://wiki.archlinux.org/index.php/Bumblebee|Bumblebee Archlinux Wiki]]
       
[[https://forum.manjaro.org|Manjaro Forum]]


   
=== PRIME ===


==== Launch applications with nvidia gpu ==== 

`prime-run aplicacion` 

==== Editing applications launchers to use the gpu nvidia ====
        
_*Search for the*_ `*.desktop` _*file of those applications you want to be launched with the gpu nvidia*_

_*Edit the file in the line*_ `exec` _*and add*_ `prime-run` _*at the beggining of the command.*_
      
==== More Info ====
      
[[https://wiki.archlinux.org/index.php/NVIDIA_Optimus#Use_switchable_graphics|Nvidia Optimus Archliinux Wiki]]

[[https://forum.manjaro.org|Manjaro Forum]]

   

== PRIME with FREE drivers == 

  _*Thanks to a@andruscodex*_
      
_*By default all FREE drivers support Prime.*_

_*Using the enviroment variable*_ `DRI_PRIME` _*we can use which gpu use to launc specifics applications.*_

_*By default the integrated gpu*_ `igpu` _*are use as primary gpu with valor*_ `0` _*and the dedicated gpu*_ `dgpu` _*with the valor*_ `1`.


==== To launch an application with the dgpu ====

`DRI_PRIME=1 aplicacion`


==== Examples ==== 


==== To launch `google-chrome` with the dgpu ==== 

`DRI_PRIME=1 google-chrome`

==== To launch `google-chrome` with the igpu ====
		
`DRI_PRIME=1 google-chrome`

==== To check the list of availables gpu ==== 
  
`xrandr --listproviders`

==== Verify the info of the gpu installed ==== 


===== Install glxinfo =====

`sudo pacman -S glxinfo`

===== Igpu =====

`DRI_PRIME=0 glxinfo`

===== Dgpu =====

`DRI_PRIME=1 glxinfo`



== Optimus switch ==

_*Using this option your machine wil use only one gpu. In order to change between gpus it need to run a script ang reboot.*_


=== Combo Intel/Nvidia ===


==== LightDM Users ==== 

	https://github.com/dglt1/optimus-switch

==== GMD Users ====

	https://github.com/dglt1/optimus-switch-gdm

==== SDDM Users ====

	https://github.com/dglt1/optimus-switch-sddm


=== Combo AMD/Nvidia ===


==== LighDM Users ====

	https://github.com/dglt1/optimus-switch-amd

==== GDM Users ====

	https://github.com/dglt1/optimus-switch-amd-gdm

==== SDDM Users ====

	https://github.com/dglt1/optimus-switch-amd-sddm
