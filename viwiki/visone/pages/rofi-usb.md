
= *Rofi-Usb* =

_This script is a fork of:_

[[https://github.com/luyves/polybar-rofi-usb-mount/blob/master/rofi-usb-mount.sh|Rofi-Usb]]

== Description ==

_This script allows you to mount/umount devices_


== Dependencies ==

*  Usbutils
*  rofi
*  dmenu


== Code: ==


{{{


#!/bin/bash


usbcheck(){ \
    mounteddrives="$(lsblk -rpo "name,type,size,mountpoint" | grep -v "nvme0n1" | grep -v 'sda' | grep -v "sdb" | awk '$2=="part"&&$4!=""{printf "%s (%s)\t  ",$1,$3}')"
    if [ $(echo "$mounteddrives" | wc -w) -gt 0 ]; then
        echo "  #  $mounteddrives"
    else
        if [ $(echo "$usbdrives" | wc -w) -gt 0 ]; then
            echo "  #  "
        else
            echo ""
        fi
    fi
}

mountusb(){ \
    chosen=$(echo "$usbdrives" | rofi -dmenu -show run -l 5 -i -p "Mount which drive?" | awk '{print $1}')
    mountpoint=$(udisksctl mount --no-user-interaction -b "$chosen" 2>/dev/null) && notify-send "💻 USB mounting" "$chosen mounted to $mountpoint" && exit 0

}

umountusb(){ \
    chosen=$(echo "$mounteddrives" | rofi -dmenu -show run -l 5 -i -p "Unmount which drive?" | awk '{print $1}')
    mountpoint=$(udisksctl unmount --no-user-interaction -b "$chosen" 2>/dev/null) && notify-send "💻 USB unmounting" "$chosen mounted" && exit 0
    udisksctl power-off --no-user-interaction -b "$chosen"
}

umountall(){ \
    for chosen in $(echo $(lsblk -rpo "name,type,size,mountpoint" | grep -v 'sda' | awk '$2=="part"&&$4!=""{printf "%s\n",$1}')); do
        udisksctl unmount --no-user-interaction -b "$chosen"
        udisksctl power-off --no-user-interaction -b "$chosen"
    done
}


usbdrives="$(lsblk -rpo "name,type,size,mountpoint" | grep -v 'sda' | awk '$2=="part"&&$4==""{printf "%s (%s)\n",$1,$3}')"
mounteddrives="$(lsblk -rpo "name,type,size,mountpoint" | grep -v 'sda' | awk '$2=="part"&&$4!=""{printf "%s (%s)\n",$1,$3}')"

op="Check\nMount\nUmount\nUmount-all"
ch=$(echo -e "$op" | rofi -dmenu  -i -l 4 -p 'USB AutoMount')
case "$ch" in
       Check)
        usbcheck
        ;;
       Mount)
        if [ $(echo "$usbdrives" | wc -w) -gt 0 ]; then
            notify-send "USB drive(s) detected."
            mountusb
        else
            notify-send "No USB drive(s) detected." && exit
        fi
        ;;
       Umount)
        if [ $(echo "$mounteddrives" | wc -w) -gt 0 ]; then
            notify-send "USB drive(s) detected."
            umountusb
        else
            notify-send "No USB drive(s) to unmount." && exit
        fi
        ;;
       Umount-all)
        if [ $(echo "$mounteddrives" | wc -w) -gt 0 ]; then
            notify-send "Unmounting all USB drives."
            umountall
        else
            notify-send "No USB drive(s) to unmount." && exit
        fi
         ;;
esac

}}}
