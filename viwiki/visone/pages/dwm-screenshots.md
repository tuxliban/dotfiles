
= *Dwm-Screenshot* =

 _Visone Screenshot Script_
 
This scrtip allow you to make a screeshot with different options:

== Options ==

=== Desktop === 

Screenshot of full desktop

=== Region ===

Screenshot of the region marked by the mouse	

=== Multiple Region ===

Screenshot of multiples regions marked by the mouse

=== First monitor ===

Screenshot of the firs monitor

=== Second monitor ===

Screnshot of the second monitor
	

=== Sign screenshots ===

Sign screenshots with imagemagic


== Dependencies == 

*  Imagemagick

*  dmenu 

*  herbe 



=== Code: ===

{{{

#!/bin/sh


dir="$HOME/wallpapers/Screenshots" 
file="$(date +%Y-%m-%d-%M-%S).png"
tfile="/tmp/capture.png"
dm="dmenu -b -X 635 -Y 20 -W 660 -l 2"


 menu() {

ch=$( printf "WindowFocus\nArea\nMultiArea\nMonitor1\nMonitor2\nClipboardArea\nClipboardDesk" | $dm -p " ScreenShots")
case "$ch" in
	WindowFocus)dsk ;;
	Area) rg ;;
	MultiArea) mr ;;
	Monitor1) fm ;;
	Monitor2) sm  ;;
	ClipboardArea) cla ;;
	ClipboardDesktop) cld ;;
esac
}

sign() {

	convert $tfile -gravity South -font MesloLGS-NF-Italic  -weight Bold -background transparent -fill 'srgb(169,169,169)' -pointsize 30 -annotate +750+4 '@visone_selektah' $dir/$file
}

not() {

	herbe "A New ScreenShoot Has Been Taken " " "$file" "
}


cla() {

	import png:- | xclip -selection clipboard -t image/png
	sign
	not
}


cld() {
	import -window root png:- | xclip -selection clipboard -t image/png
	sign
	not
}


dsk() {
	sleep 3
	import -window $(xdotool getwindowfocus) $tfile	
	sign
	not
}

rg() {
	import $tfile 
	sign
	not
}

mr() {
	import -snaps 3 $tfile 
	sign
	not
}

fm() {
	sleep 3 
	import -window root -crop 1920x1080+0+0 $tfile
	sign
	not
}

sm() {
	sleep 3 
	import -window root -crop 1920x1080+1920+0 $tfile 
	sign
	not
}


screen() {
	sleep 3 
	import -window root $tfile
	sign
	not

}

case $1 in
	-s) screen ;;
	-m) menu ;;
esac
	
}}}
