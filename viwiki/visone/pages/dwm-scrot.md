
= *Dwm-Scrot Scrip* =


== Description ==

This script allows you to make a screenshoot of your desktop named with the date and move it to a dir of your choice and show you a notification of it
	

== Dependencies ==

*  scrot
*  dunst

=== Code: ===

{{{

#!/bin/bash

scrot -d 5 '%Y-%m-%d-%H:%M-scrot.png' -e 'mv $f ~/Imágenes/Screenshots/' 
notify-send "Screnshoot" "A New Screenshot Has Been Made\n It's in ~/Imágenes/Screenshots/" 
}}}
