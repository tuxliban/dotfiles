
== *Dragon* ==

_Drag & Drop source/sink for X & Wayland_

[[https://github.com/mwh/dragon|Dragon Git Repo]]


=== *Use with Ranger* ===


Just add this line in your `rc-conf` in Ranger

`map <C-d> shell dragon -a -x %p --and-exit`


* Edit the keybinding to it, by default uses Ctrl+d
* It works with de visual and normal mode

