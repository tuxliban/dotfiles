
= *Pulsemixer* =


_*This script is a terminal alternative to manage pulseaudio or pipewire server*_
	  
	  [[https://github.com/GeorgeFilipkin/pulsemixer|Pulsemixer Git Repo]]


== Usage of pulsemixer ==


{{{
-h, --help            show this help message and exit
-v, --version         print version
-l, --list            list everything
--list-sources        list sources
--list-sinks          list sinks
--id ID               specify ID, default sink if no ID
--get-volume          get volume for ID
--set-volume n        set volume for ID
--set-volume-all n:n  set volume for ID, every channel
--change-volume +-n   change volume for ID
--max-volume n        set max volume to n 
--get-mute            get mute for ID
--mute                mute ID
--unmute              unmute ID
--toggle-mute         toggle mute for ID
--server              choose the server to connect to
--color n             0 !color, 1 actual color, 2 full-color
--no-mouse            disable mouse support
--create-config       generate configuration file
}}}
