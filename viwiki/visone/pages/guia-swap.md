
=*Guia Swap Config*=

_*The size assinged to swap is relative and depend of a lot of factors, workflow, gaming, video rendering, home use,etc. Personally I recomend this options for a normal workflow.*_ 

=== For machines with === 

*  4GiB RAM or less : 8GiB swap
*  8GiB RAM or less : 4GiB swap
*  16GiB RAM with an intensive use : 4GiB swap

_Note:_

_SSD / NVME_ 

- _*It is recomended to use*_ `swapfile`

- _*It is posible to use other HDD or even an USB to store swap but the system will have less performance when the system started to use it.*_
 

==Swap Partition== 
	

_*Make a new partition using any existing available space or resizing a existing partition*_  


==== Set the new partition as SWAP ==== 

`sudo mkswap /dev/sdXN`

_Edit (sdXN) with your partition dev name_


==== Activating SWAP ====

`swap on /dev/sdXN` 
		
_Edit (sdXN) with your partition dev name_


==== Add SWAP partition to /etc/fstab ==== 


===== Find and note the UUID of the SWAP partition =====

`sudo blkid | grep "/dev/sdXN"`

_Edit (sdXN) with your partition dev name_


===== As root edit fstab and add this line at the end. =====
		
`UUID=<UUID string noted before> none swap defaults 0 0`

	
===== Save the file and reboot the system. =====
	
	
== Swapfile ==


==== Make a SWAPFILE ====
	
`sudo dd if=/dev/zero of=/swapfile bs=1G count=4 status=progress`

==== Where: ====

*  bs=1G  1GB
*  count=4 4*$bs
*  This will make a 4GB swapfile


==== Assing permissions to swapfile ==== 

`sudo chmod 600 /swapfile`


==== Formating swapfile ====

`sudo mkswap /swapfile`


==== Activating swapfile ==== 

`sudo swapon /swapfile`


==== Add swapfile to fstab ====


===== Edit as root fstab file and add this line at the end. =====

`/swapfile none swap defaults 0 0`

===== Save the file and reboot the system ===== 
