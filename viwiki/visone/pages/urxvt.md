
= *URxvt Config* =

_*This is my personal config of Urxvt*_

== *Dependencies* ==

* _*Rxvt-unicode*_
* _*Uxvt-perls*_



== *Configuration* ==

* *_Xinitrc file*_

_*Add this line to*_ `~/.xinitrc` _*file to launc the daemon*_ `Urxvtd`

`urxvtd --quiet --opendisplay --fork &`

* _*Add this to*_ `~/.Xresources` _*file*_

{{{


!! URxvt Appearance
URxvt.font: xft:MesloLGS NF:style=Regular:size=12
URxvt.boldFont: xft:MesloLGS NF:style=Bold:size=12
URxvt.italicFont: xft:MesloLGS NF:style=Italic:size=12
URxvt.boldItalicFont: xft:MesloLGS NF:style=Bold Italic:size=12
URxvt.termName:rxvt-unicode-256colors
Xft.antialias:  true
Xft.rgba:       rgb
Xft.hinting:    true
Xft.hintstyle:  hintsfull
Xft.autohint:   false
Xft.lcdfilter:  lcddefault
URxvt.internalBorder: 8
URxvt.letterSpace: 0
URxvt.lineSpace: 0
!URxvt.geometry: 92x24
!URxvt.cursorBlink: false
URxvt.cursorUnderline: false
URxvt.saveline: 2048
URxvt.scrollBar: false
URxvt.scrollBar_right: false
URxvt.urgentOnBell: true
URxvt.depth: 32
URxvt.inheritPixmap: true
URxvt.iso14755: false
URxvt.print-pipe: "cat > /dev/null"

!! Common Keybinds for Navigations
URxvt.keysym.Shift-Up: command:\033]720;1\007
URxvt.keysym.Shift-Down: command:\033]721;1\007
URxvt.keysym.Control-Up: \033[1;5A
URxvt.keysym.Control-Down: \033[1;5B
URxvt.keysym.Control-Right: \033[1;5C
URxvt.keysym.Control-Left: \033[1;5D

!! Perl Extensions
URxvt.perl-ext-common:default,selection-to-clipboard,keyboard-select,font-size,tabbedalt,url-select-plus,-confirm-paste,vtwheel
URxvt.clipboard.autocopy: true
URxvt.copyCommand: xsel -b 
URxvt.pasteCommand: xsel -o
URxvt.keysym.Control-V: eval:paste_clipboard
URxvt.keysym.Control-C: eval:selection_to_clipboard
URxvt.keysym.Control-Escape: perl:keyboard-select:activate
URxvt.keysym.Control-s: perl:keyboard-select:search
URxvt.keysym.Control-u: perl:url-select:select_next
URxvt.keysym.M-u: perl:url-seleca-true:select_next
URxvt.url-select-plus.launcher: /home/visone/scripts/vi-nuke
URxvt.url-select-plus.altlauncher: firefox 
URxvt.url-select-plus.underline: true
URxvt.url-select-plus.autocopy: true
URxvt.url-select-plus.mediaplayer: mpv
URxvt.url-select-plus.imgviewer: display
URxvt.underlineURLs: true
URxvt.urlButton: 1
URxvt.keysym.Alt-plus: font-size:increase
URxvt.keysym.Alt-minus: font-size:decrease
URxvt.keysym.S-plus: font-size:incglobal
URxvt.keysym.S-minus: font-size:decglobal
URxvt.keysym.Alt-equal: font-size:reset
URxvt.keysym.Alt-slash: font-size:show

!TABS
URxvt.tabbedalt.autohide: true
URxvt.tabbedalt.tabbar-fg: 3
URxvt.tabbedalt.tabbar-bg: 0
URxvt.tabbedalt.tab-fg:    6
URxvt.tabbedalt.tab-bg:    0
URxvt.tabbedalt.active-bg: 0
URxvt.tabbedalt.actives-fg: 9
URxvt.tabbedalt.new-button: false
URxvt.tabbedalt.tab-numbers: false
URxvt.tabbedalt.tabcmds.1: S|shell
URxvt.tabbedalt.tabcmds.2: F|nnn|nnn
URxvt.tabbedalt.tabcmds.3: N|vim|vim
!URxvt.tabbedalt.session: F|S

  }}}
