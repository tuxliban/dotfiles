
= *Mpv Conf* =

	_Visone mpv config file_

=== Code: ===

{{{


##################
# video settings #
##################

vo=gpu
profile=gpu-hq
hwdec=auto
save-position-on-quit

##################
# audio settings #
##################

# Volume
volume-max=125 

# Channels
#audio-channels=auto

##################
# other settings #
##################

#font
sub-font=Iosevka

# Display English subtitles if available.
slang=eng,en

# Play Finnish audio if available, fall back to English otherwise.
#alang=fi,en

# You can also include other configuration files.
#include=/path/to/the/file/you/want/to/include

# youtube-dl port to yt-dlp
script-opts=ytdl_hook_path=/usr/bin/yt-dlp

# Yt-dlp options
ytdl-format=bv*[height=720][ext=mp4][fps=30]+ba/b

}}}
