
= *Dracut Emergency Shell* =

_*This procedure can be use to get into a shell to fix any problem while having acces to grub*_


=== *Process* ===

* _*While in grub pulse*_ `e` _*to edit grub boot options*_
* _*Add*_ `rd.break` _*to kernel boot options line*_
* _*Pulse*_ `Ctrl*x` _*to boot into dracut emergency shell*_
* _*Mount your root partition:*_

	1.- _*Ext4*_
		* `mount -o remount, rw /sysroot`
	2.- _*Btrfs*_
		* _*Set btrfs mounting options to*_ `$BTRFS_OPTIONS`
		* `mount -o $BTRFS_OPTIONS /sysroot`
* _*Use chroot to get into the system with*_ `chroot /sysroot` 
* _*Also can be launch commands from outside chroot with*_ `chroot /sysroot <command>`
