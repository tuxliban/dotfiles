
= *Firefox* =


== Enable Vaapi Hardware acceleration ==


==== Enter configuration page ====
	
	`about:config`


==== Flags ====    

	* `#media.ffmpeg.vaapi-drm-display.enabled` `true`
	* `#media.ffmpeg.vaapi.enabled` `true`
	* `#media.ffvpx.enabled` `false`
	* `#media.rdd-vpx.enabled` `false`
	* `#media.navigator.mediadatadecoder_vpx_enabled` `true`
