
= *FFMPEG Commands* =


== Ffmpeg Desktop Record ==

_*This options are for cpu record*_
	


===Options===


==== Resolution ====
	
	`-s 1920x1080`


==== Framerate ====
	
	`-framerate 30`


==== Video ====
	
	`-f x11grab -i :0.0`


==== Audio ====
	
	`-f pulse -i @DEFAULT_SINK@`


==== Video codec ====
	
	`-c:v libx264`


==== Video bitrate ====
	
	`-b:v 800k`


==== Audio codec ====
	
	`-c:a aac`


==== Audio bitrate ====
			
	`-b:a 160k`



=== Example ===


* _*Record the desktop with FHD resolution, framerate 30, audio default sink, video codec h264, video bitrate 800k, audio codec aac, audio bitrate 160k*_
		
		
`ffmpeg  -s 1920x1080 -framerate 30 -f x11grab -i :0.0 -f pulse -i @DEFAULT_SINK@ -c:v libx264 -b:v 800k -c:a aac -b:a 128k  $file`
