
==*Iwctl*==

_*This applications needs root permissions*_

_*Use this manual to enable and activate your wifi from terminal*_


=== 1. Enter iwd mode ===

`sudo iwctl`

=== 2. Found devices names ===

`device list`

=== 3. Wifi scan ===

`station <device> scan`

=== 4. Get networks list available ===

`station <device> get-networks`

=== 5. Conect wifi ===

`station <device> connect <wifi>`
