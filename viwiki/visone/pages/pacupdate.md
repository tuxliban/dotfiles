
=*PacUpdate Script*=

	_Visone PacUpdate Script_

== Description ==

This script allows you to show how many pkgs pending to update you have


== Dependencies ==

*  pacman-contrib 
*  paru
		
== Options ==		

*  if you use other AUR-Helper change it in the script


== Code: ==

{{{


#!/bin/bash

updates_arch=$(checkupdates 2> /dev/null | wc -l );
updates_aur=$(paru -Qum 2> /dev/null | wc -l)
updates=$(("$updates_arch" + "$updates_aur"))
echo " └ $updates ┘ " 

}}}
