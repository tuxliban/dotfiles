
= *Alias-Search script* =

_This scripts allows you to search in your .aliases file for any alias by string_


== Dependencies: ==

	*  sed
	*  awk
	*  grep
	* ~/.aliases file
	


== Code: ==


{{{

#!/bin/sh

# Usage:
# It can used by an alias `as <string>` or by script `script-name <string>`

awk  '!/#/ {print}' ~/.aliases | sed -E "s/alias (.+)='(.+)'/\1 --> \2/" | grep "$1" 

  }}}
