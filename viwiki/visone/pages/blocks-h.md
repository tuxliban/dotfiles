
=*Dwmblocks Conf Blocks.h*=

 _Visone Dwmblock config_

== Dependencies ==  
	
*  ttf-joypixels-font


=== Scripts ===

*  [[vol|Volume]]

*  [[cputemp|CpuTemp]]

*  [[openweather|OpenWeather]]

*  [[pacupdate|Pacupdate]]

*  [[system|System]]

*  [[clock|Clock]]


== Code: ==

{{{


//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/	 	/*Update Interval*/	/*Update Signal*/
    {" 📡 ", "$HOME/.config/dwmblocks/scripts/openweather",	3600,	7},

	{" 🔥 ", "$HOME/.config/dwmblocks/scripts/cputemp",		        1,		            2},

	{" 📦 ", "$HOME/.config/dwmblocks/scripts/pacupdate",		3600,		        9},
	
	{" 💻 ", "$HOME/.config/dwmblocks/scripts/system",	        6,		            1},

	{" 🔊 ", "$HOME/.config/dwmblocks/scripts/vol",			0,		            10},

	{" 🕑 ", "$HOME/.config/dwmblocks/scripts/clock",			60,		            0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';

}}}
