
== *Grub UEFI Reinstallation* ==  

_Manual for grub reinstallation in Arch based distros_  

_Thanks to_ [[t.me/manjarolinuxes|ManjaroEs Telegram Group]]
	


=== *Preparation* ===


* Download and flash the live iso into a usb device

In this manual we'll use [[https://www.ventoy.net/en/index.html|Ventoy]].  
It's a multiboot application with persistence capabilities.
There're many other options like etcher or even the cli command *dd* but Ventoy is one of the more reliable with uefi systems and easy to use after install.  
Check Ventoy manual [[pages/ventoy|here]].



*  Live Linux Iso

It's recommendable to use the same live iso that our broken system, it could be any linux distro but with the different chroot applications and methods used by different distros this manual will became so much longer and complicated.


=== *Reinstallation* ===


*  Boot up our system with the live iso and open a terminal.  
*  Make sure that the partitions of the installed system are umounted, use gparte,partitionmanager or the terminal to do so.  
*  Chrooting the installed system.  
   -  Arch
   	*  List the partitions in our system with *lsblk* and noted the boot (if exist) and root partitions dev name
	*  Mount root partition in /mnt `sudo mount /dev/$root /mnt`
	*  Mount boot partition in /mnt/boot/efi `sudo mount /dev/$boot /mnt/boot/efi`
	*  Enter the system using chroot, Be advise that different distros have different methods to use chroot, check yours, in this case we'll use arch way `sudo arch-chroot /mnt`
	*  Reinstall grub `grub-install --target=x86_64-efi --efi-directory=/boot/efi --botloader-id=<mi-grub>`
