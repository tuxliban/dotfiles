
== *Brightness Changer Script* ==

_*This script allows you to manage brightness using xrandr*_

=== Dependencies: ===

	* xrandr
	
=== Code: ===

{{{


#!/bin/sh

# Brightness changer using xrandr
# Usage: 
# bright + to up brightness in 0.1 step
# bright - to down brightness in 0.1 step

# Change the output name, use xrandr to know yours
output="eDP-1"

case "$1" in

	+)
		xrandr --output $output --brightness $(echo "$(xrandr --verbose |grep Brightness |grep -o '[0-9].*')+0.1" | bc) ;;

	-)
		xrandr --output $output --brightness $(echo "$(xrandr --verbose |grep Brightness |grep -o '[0-9].*')+0.1" | bc) ;;

esac

  }}}
