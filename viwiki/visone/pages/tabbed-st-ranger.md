
= *Tabbed ST Ranger Script* =

 _Visone Tabbed Script_
 
 
== Description ==
 
 This script allows you to launch a st terminal and the file browser ranger in the same window separated in tabs

== Dependencies ==

*  abbed
*  St
*  Ranger

=== Code: ===

{{{

#!/bin/sh


tb=$(tabbed -c -d  -r 2 st -w x); st -w "$tb" ranger ; "$tb"

}}}
