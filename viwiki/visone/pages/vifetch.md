
= *Vifetch* =

*Visone fetch script*

== *Code* ==

{{{


#!/bin/sh

. /etc/os-release

kernel=$(cat /proc/sys/kernel/osrelease)
os=${PRETTY_NAME:-$ID-$VERSION_ID}
hn=$(cat /etc/hostname)
shell=$(printf $(basename $SHELL))
cpu=$(awk -F ": " 'NR==5 {print $2}' /proc/cpuinfo)
gpu=$(awk -F'"' '/Chipset/{print $2}' /var/log/Xorg.0.log | cut -c -18)
rsl=$(sed 's/,/x/g' /sys/class/graphics/fb?/virtual_size)
tr=$(printf "$TERM")
font="$(awk -F'"' '/font/{print $2}' "${HOME}/.config/gtk-2.0/gtkrc")"
wm="$( awk '/dwm/{print}' "${HOME}/.config/X11/xinitrc")"
mem=$(free -m | awk '/^Mem:/ {print $3 "MiB / " $2 "MiB"}')
st=$(df -h / | awk '/dev/{print $3" / "$2}')

#init & pkg
case "$hn" in
	void*) init=$(cat /proc/1/comm)
	       pkg="$(xbps-query -l | wc -l)" ;;
	venom*) init=$(eval /sbin/init --version 1>/dev/null && echo SysVinit)
		pkg=$(ls $(pkgadd --print-dbdir) | wc -l) ;;
esac





full() {
## Venom-Linux
printf %"$(tput cols)"s |tr " " "="

cat <<EOF
·> Hostname:			${hn}		
·> Processor (Cpu):		${cpu}	
·> Graphics Processor (GPU):	${gpu}
·> GPU Resolution:		${rsl}
·> Ram Used:			${mem}
·> Size subvol @:		${st} 
·> Linux Distribution:		${os}
·> Linux Kernel:		${kernel}
·> System Init:			${init}
·> Packages installed (spkg):	${pkg}
·> User Shell: 			${shell}
·> Terminal:			${tr}
·> Window Manager:		${wm}
EOF

printf %"$(tput cols)"s |tr " " "="

}

full

  }}}
