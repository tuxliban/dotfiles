
= *Pulsemixer* =


_*This script is a terminal alternative to manage pulseaudio or pipewire server*_
	  
	  
	  [[https://github.com/GeorgeFilipkin/pulsemixer|Pulsemixer Git Repo]]


== Config ==

{{{
;; Goes into ~/.config/pulsemixer.cfg 
;;$XDG_CONFIG_HOME respected
;; Everything that starts with "#" or ";" is a comment
;; For the option to take effect simply uncomment it

[general]
step = 1
step-big = 10
; server =

[keys]
;; To bind "special keys" such as arrows see "Key constant"
;; https://docs.python.org/3/library/curses.html#constants
; up        = k, KEY_UP, KEY_PPAGE
; down      = j, KEY_DOWN, KEY_NPAGE
; left      = h, KEY_LEFT
; right     = l, KEY_RIGHT
; left-big  = H, KEY_SLEFT
; right-big = L, KEY_SRIGHT
; top       = g, KEY_HOME
; bottom    = G, KEY_END
; mode1     = KEY_F1
; mode2     = KEY_F2
; mode3     = KEY_F3
; next-mode = KEY_TAB
; prev-mode = KEY_BTA		; mute      = m
; lock      = ' '  ; 'space', quotes are stripped
; quit      = q, KEY_ESC

[ui]
; hide-unavailable-profiles = no
; hide-unavailable-ports = no
; color = 2    ; 0 no color, 1 color actual, 2 full-color
; mouse = ye	
[style]
;; Pulsemixer will use these characters to draw interface
;; Single characters only
; bar-top-left       = ┌
; bar-left-mono      = ╶
; bar-top-right      = ┐
; bar-right-mono     = ╴
; bar-bottom-left    = └
; bar-bottom-right   = ┘
; bar-on             = ▮
; bar-on-muted       = ▯
; bar-off            = -
; arrow              = ' '
; arrow-focused      = ─
; arrow-locked       = ─
; default-stream     = *
; info-locked        = L
; info-unlocked      = U
; info-muted         = M  ; 🔇
; info-unmuted       = M  ; 🔉

[renames]
;; Changes stream names in interactive mode 
;;regular expression are supported
; 'default name example' = 'new name'
; '(?i)built-in .* audio' = 'Audio Controller'
; 'AudioIPC Server' = 'Firefox'

}}}
