
= *Yydl-Mpv* =

_Visone youtube player and downloade_


== Descriptions ==

This scripts offers a menu with different options to play o download videos of youtube and ther platforms supported by youtube-dl


== Dependencies ==

* yt-dlp
* dmenu
* mpv
* xclip


== Code: ==


{{{


#!/bin/sh


# This script is open-source so you can make whatever you want with it
#
# This script need some dependencies:
#  1. MPV 
#  2. yt-dlp 
#  3. Xclip
#  4. Dmenu
#
# This script allow you to download or play videos of all sites compatibles with yt-dlp.
# More info about yt-dlp in https://github.com/ytdl-org/yt-dlp
#
# Configuration
#
#   1. Terminal
#  		Edit the line: term="termite" and change it for any teminal you use
#
#   2. Directory
#		Edit the lines:
#			dr=/home/$USER/ytdl-mpv/
#			mkdir /home/$USER/ytdl-mpv
#       Change them to any path or dir you want all downloads be store
# 
#   3. Menu format
#		Edit the lines with:
#			rofi -dmenu -theme Pop-Dark
#		Change it to any format, theme you want
#


. "${HOME}/.cache/wal/colors.sh"

dm="dmenu -b -X 370 -Y 20 -W 1200 -l 5"

# Setting Terminal
term="st"

# Using xplit to input video
url=$(xclip -o -select c)

# Path to Downloads
dr=/home/$USER/ytdl-mpv/
if [ -d "$dr" ]
	then
		cd $dr
	else
		mkdir /home/$USER/ytdl-mpv
		cd $dr
fi

# Menu Options
	funcMO() {
		options="DownloadMenu\nPlayMenu"
		mo=$(echo "$options" | $dm -p  " Menu " )

		case "$mo" in
			DownloadMenu) funcDM ;;
			PlayMenu) funcPM ;;
		esac
	}


# Download Menu func
funcDM() {
	options="DownloadVideo\nDownloadAudio\nDownloadSubs\nDownloadPlaylist"
	dm=$(echo "$options" | $dm -p " Descargar Menu")

	case "$dm" in 
		DownloadVideo) funcDVideo ;;
		DownloadAudio) funcDAudio ;;
		DownloadSubs) funcDSubs ;;
		DownloadPlaylist) funcDPlaylist ;;
	esac
}

# Play Menu func
funcPM() {
	options="PlayVideo\nPlayAudio\nPlayPlaylist"
	pm=$(echo "$options" | $dm -p " PlayMenu" )

	case "$pm" in
		PlayVideo) funcPVideo  ;;
		PlayAudio) mpv -ytdl-format="ba" $url ;;
		PlayPlaylist) funcPPLM ;;
	esac
}


# func Download Video

funcDVideo() {
	options="1.BestQualityVideoAudio\n2.Quality1080p\n3.Quality720p\n4.Quality480p\n5.ChooseQuality\n6.Exit"
	dv=$( echo "$options" | $dm -p " Descargar Video" )

	case "$dv" in

		1.BestQualityVideoAudio) 
			"$term" -e  "yt-dlp -f bv+ba/b $url " ;
			funcOtherDVideo ;;

		2.Quality1080p) 
			 "$term" -e "yt-dlp -f bv[height=1080]+ba/b[height=1080] $url" ; 
			 funcOtherDvideo ;;

		3.Quality720p)
			"$term" -e "yt-dlp -f bv[height=720]+ba/b[height=720] $url" ; 
			funcOtherDvideo ;;

		4.Quality480p)
			"$term" -e "yt-dlp -f bv[height=480]+ba/b[height=480] $url" ; 
			funcOtherDvideo ;;

		5.ChooseQuality) funcDCQ ;;

		6.Exit) exit ;;
	esac
}

# func Choose Quality Download Video

funcDCQ() {
	
	options=$(yt-dlp --list-formats "$url" | grep -v "audio only" | awk 'NR==4 , NR==end {print $1 "-" $2 "-" $5}' | $dm -p "Elige calidad")
	qy=$(echo "$options" | awk '{print $1}')
	mpv -ytdl "$qy" "$url"
	funcOtherDVideo
}


# func Download Audio

funcDAudio(){
	options="1.BestQualityMP3\n2.ChooseQualityBitrate\n3.ChooseBetweenAvailablesFormats\n4.Exit"
	da=$(echo "$options" | $dm -p " Descargar Audio")

	case "$da" in
		1.BestQualityMP3)
			"$term" -e "yt-dlp --extract-audio --audio-quality 0 --audio-format mp3 $url" ;  
			funcOtherDAudio ;;

		2.ChooseQualityBitrate) funcDQB ;;

		3.ChooseBetweenAvailablesFormats) funcCBAF  ;;
		
		4.Exit) exit ;;
	esac
		
}

# func Choose Quality Bitrate

funcCQB() {
	br=$( $dm -p "Choose Bitrate between 0-9 / 0=b 9=worst")
	"$term" -e "yt-dlp --extract-audio --audio-quality "$br" --audio-format mp3"
	funcOtherDAudio 
}

# func Choose Between Availables Formats

funcCBAF() {
	format=$(yt-dlp --list-formats $url | grep "audio only" | awk '{print $1 "-" $2 "-" $6}' | $dm -p " Posiles Formatos " )
	fm=$(echo "$format" | awk '{print $1}')
	"$term" -e "yt-dlp "$fm" $url"
	funcotherDAudio
}


# func Download Subtitles menu

funcDSubs() {
	options="OriginalSubs\nSubsFormatLanguage\nAllSubs\nExit"
	sm=$( echo "$options" | $dm -p "Descargar Subs Menu")

	case "$sm" in
		OriginalSubs)
			"$term" -e "yt-dlp --write-sub --skip-download $url" ; 
			funcOtherDSub ;;
			
		SubsFormatLanguage) funcSFL ;;
		
		AllSubs) "$term" -e "yt-dlp --all-subs --skip-download $url" ; 
			 funcOtherDSub ;;
		
		Exit) exit ;;
	esac
}

# func Choose Subs Format

funcSFL() {
	opt="SRT\nASS"
	fopt=$(echo "$opt" | $dm -p " Elige Formato Subs ")
	opt1="ES\nEn\nFR\nITA\nGER"
	lopt=$(echo -e "$opt1" | $dm -p " Elige Lang Subs")
	"$term" -e "yt-dlp --sub-format $fopt --sub-lang $lopt --skip-download $url" ;
	funcOtherDSub
}

# func Download PlayList

funcDPlaylist() {
	options="VideoAudio\nAudio\nByDate\nExit"
	dp=$(echo "$options" | $dm -p " PlayList Menu")

	case "$dp" in
		VideoAudio)funcDVideo ; 
			funcOtherDPlaylist ;;
		Audio) funcDAudio ; 
			funcOtherDAudio ;;
		ByDate) funcPPLBD ;;
		Exit) exit ;;
	esac
}

# func Download Other Video

funcOtherDVideo() {
	options="DownloadOtherVideo\nExit"
	odv=$(echo "$options" | $dm -p "Exit Menu")

	case "$odv" in
		DownloadOtherVideo) funcDVideo ;;
		Exit) funcMO ;;
	esac
}

# func Download Other Audio

funcOtherDAudio() {
	options="DownloadOtherAudio\nExit"
	oda=$(echo "$options" | $dm -p "Exit Menu")

	case "$oda" in
		DownloadOtherAudio) funcDAudio ;;
		Exit) funcMO ;;
	esac
}

# func Download Other Sub

funcOtherDSub() {
	options="DownloadOtherSub\nExit"
	ods=$(echo "$options" | $dm -p "Exit Menu")

	case "$ods" in
		DownloadOtherSub) funcDSub ;;
		Exit) funcMO ;;
	esac
}

funcPVideo() {
	options="1.BestQualityVideoAudio\n2.Quality1080p\n3.Quality720p\n4.Quality480p\n5.ChooseQuality\n6.Exit"
	dv=$( echo "$options" | $dm -p " Descargar Video" )

	case "$dv" in

		1.BestQualityVideoAudio) 
			mpv -ytdl-format="bv+ba/b" "$url" ;  
			funcOtherPVideo ;;

		2.Quality1080p) 
			mpv -ytdl-format="bv[height=1080]+ba/b[height=1080]" "$url" ; 
			funcOtherPvideo ;;

		3.Quality720p)
			mpv -ytdl-format="bv[height=720]+ba/b[height=720]" "$url" ; 
			funcOtherPvideo ;;

		4.Quality480p)
			mpv -ytdl-format="bv[height=480]+ba/b[height=480]" "$url" ; 
			funcOtherPvideo ;;

		5.ChooseQuality) funcPCQ ;;

		6.Exit) exit ;;
	esac
}

funcOtherPVideo() {
	options="PlayOtherVideo\nExit"
	odv=$(echo "$options" | $dm -p "Exit Menu")

	case "$odv" in
		PlayOtherVideo) funcPVideo ;;
		Exit) funcMO ;;
	esac
}


# func Choose Quality Play Video

funcPCQ() {
	options=$(yt-dlp --list-formats "$url" | grep -v "audio only" | awk 'NR==4 , NR==end {print $1 "-" $2 "-" $5}' | $dm -p "Elige calidad")
	qy=$(echo "$options" | awk '{print $1}')
	mpv -ytdl "$qy" "$url" ;
	funcOtherPVideo
}	


# func Play Playlist

funcPPLM() {
	options="VideoAudio\nAudio"
	ppl=$(echo "$options" | $dm -p "PlayList Menu")

	case "$ppl" in
		VideoAudio) mpv -ytdl-format="bv+ba/b" $url ;;
		Audio) mpv --ytdl-format="ba" $url ;;
	esac
}

# func Play Playlist by Date

funcPPLBD() {
	dt=$( $dm -p "FECHA=YYYMMDD / Elige fecha desde la que empezar a descargar" | awk '{print $1}' )
	mpv ytdl-date="$dt" "$url"
	funcOtherPlDate
}

funcMO

}}}


