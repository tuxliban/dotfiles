
= *Pulsemixer* =

_*This script is a terminal alternative to manage pulseaudio or pipewire server*_
	  
	  [[https://github.com/GeorgeFilipkin/pulsemixer|Pulsemixer Git Repo]]


== Controls ==


`j k   ↑ ↓`  -->  `Navigation`
		
`h l   ← →` -->	`Change volume`
	
`H L   Shift←  Shift→` --> `Change volume by 10`
		
`1 2 3 .. 8 9 0` --> `Set volume to 10%-100%`

`m` --> `Mute/Unmute`

`Space` --> `Lock/Unlock channels`

`Enter` --> `Context menu`

`F1 F2 F3` --> `Change modes` 

`Tab   Shift Tab` --> `Next/Previous mode` 

`Mouse click` --> `Select device or mode` 

`Mouse wheel` --> `Volume change` 

`Esc q` --> `Quit`	
