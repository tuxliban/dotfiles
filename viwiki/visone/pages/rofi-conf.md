
= *Rofi Config Rasi* =

 _Visone Rofi Conf_
 
  config.rasi file


=== Code: ===

{{{



/**
 * User: @Visone
 * Copyright: deadguy
 * Fork: @SrDaza
 * Dependencies:  python-wal  --> set colors
 */


configuration {

	modi: "drun,run,file-browser";
	display-drun:    "Activate";
	display-run:     "Execute";
/*	display-window:  "Window";*/
	show-icons:      true;
	sidebar-mode:    true;
}



@import "~/.cache/wal/colors-rofi-dark.rasi"

* {
/*    background: transparent;  */
	margin:                      1;
	padding:                     5;
}

window {

	location:	 center;
	anchor:		 center;
	height:		 50%;
	width:		 30%;
    y-offset:    5px;
	orientation: horizontal;
	children:	 [mainbox];
    border:  2px;
    border-radius: 8px; 
    border-color: @selected-active-background;
}

mainbox {
  	spacing:  1.5em;
    border:  2px;
    border-radius: 8px;
    border-color: @selected-active-background;
}


button { padding: 3px 2px; }

button selected {
	background-color: @active-background;
	text-color: 	  @normal-foreground;
}

inputbar {
	padding: 5px;
	spacing: 5px;
}

listview {
	spacing: 0.2em;
	dynamic: false;
	cycle:   true;
}

element { padding: 5px; }

entry {
	expand:         false;
	text-color:     @normal-foreground;
	vertical-align: 1;
	padding:        5px;
}

element normal.normal {
	background-color: @normal-background;
	text-color:       @normal-foreground;
}


element normal.urgent {
	background-color: @urgent-background;
	text-color:       @urgent-foreground;
}

element normal.active {
	background-color: @active-background;
	text-color:       @active-foreground;
}

element selected.normal {
	background-color: @selected-normal-background;
	text-color:       @selected-normal-foreground;
	border:           0 5px solid 0 0;
	border-color:	    @active-background;
}

element selected.urgent {
	background-color: @selected-urgent-background;
	text-color:       @selected-urgent-foreground;
}

element selected.active {
	background-color: @selected-active-background;
	text-color:       @selected-active-foreground;
}

element alternate.normal {
	background-color: @normal-background;
	text-color:       @normal-foreground;
}

element alternate.urgent {
	background-color: @urgent-background;
	text-color:       @urgent-foreground;
}

element alternate.active {
	background-color: @active-background;
	text-color:       @active-foreground;
}

}}}
