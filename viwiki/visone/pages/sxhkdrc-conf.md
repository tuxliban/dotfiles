
= *Sxhkd Configuration* =
 
  _Visone Sxhkd Config File_
  
  _sxhkdrc file_
  

== Code: ==
 
 
{{{


##  SXHKD

# Reload sxhkd
super + Escape
	pkill -USR1 -x sxhkd && notify-send "SXHKD Daemon" "Has Been Updated"

## BSPWM 

# Exit/Restart bspwm

super + shift + {e,r}
	bspc {quit,wm -r}

# close and kill
super + q
	bspc node -c

# alternate between the tiled and monocle layout
super + l
	bspc desktop -l next

	
# cycle layouts rtall,rwide
super + alt  + l
	bsp-layout cycle --layouts rwide,rtall,wide,tall,tiled && ~/scripts/notify-bspwm-layout


## Mouse wheel gaps
super + button{4,5}
	    bspc config window_gap $(( $(bspc config window_gap) {-,+} 2 ))    

# FOCUS/SWAP

# focus the node in the given direction
super + {Left,Down,Up,Right}
	bspc node -f {west,south,north,east}

# focus the next/previous node in the current desktop
control + super + {Left,Right}
	bspc node -f {next,prev}.local

# swap the nodes to direccion
super + shift + {Left,Up,Down,Right}
	bspc node -s {west,south,north,east}

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} focused:'^{1-9,10}'

# focus desktop
alt + {Right,Left}
	bspc desktop -f {next,prev}


# MOVE/RESIZE

# expand a window by moving one of its side outward
ctrl + super + {Right,Up,Down,Left}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
ctrl + super + alt + {Left,Down,Up,Right}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}



# move a floating window
super + ctrl + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

## AUDIO KEYBINDINGS

# Volume up and down
{F12,F11}
	{pactl set-sink-volume @DEFAULT_SINK@ +5% && notify-send "Volume Up 5%" "VOL $(pamixer --get-volume)%", \
	pactl set-sink-volume @DEFAULT_SINK@ -5% && notify-send "Volume Down 5%" "VOL $(pamixer --get-volume)%"
}

# APPLICATION KEYBINDINGS 
   
# launch newsboat feed reeder
super + alt + n
    termite -e newsboat

# Screenshot
Print
	scrot -d 5 '%Y-%m-%d-scrot.png' -e 'mv $f ~/Imágenes/Screenshots/' &&  notify-send "Screenshot" "A New Screenshot Has Been Made\n It's in ~/Imágenes/Screenshots/'"
	

#### Launch Vmodule-Scratchpad  {Termite/Mocp/Brave/Rofi/Pcmanfm/Telegram/Ranger/Geany/VirtManager/Pywal/kitty}
super + {Return,s,m,b,d,e,t,r,g,v,w,k,p}
	{termite,\
	bspwm-toggle-visibility qBittorrent qbittorrent, \
	termite -e "padsp mocp",\
	brave --use-gl=desktop,\
	rofi -show run -display-drun "Vi-D-Rofi-Run : " -display-run "ViRofi-Run : " -display-window "Vi-Window-Choose: ", \
	bspwm-toggle-visibility Pcmanfm pcmanfm,\
	telegram-desktop & ,\
	termite -e "ranger", \
	bspwm-toggle-visibility Geany geany, \
	bspwm-toggle-visibility Virt-manager virt-manager, \
	random-pywal, \
	kitty, \
	killall polybar; polybar vbspwm &
}	

#  Scratchpad {Termite/Kitty,Brave}
alt + { Return,t,k,b}
	{bspwm-toggle-visibility termite --take-first, \
	bspwm-toggle-visibility TelegramDesktop telegram,\
	bspwm-toggle-visibility kitty kitty --take-first, \
	bspwm-toggle-visibility Brave-browser brave
}


}}}
