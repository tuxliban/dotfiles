
= *Visnnaper Script* =

*Btrfs snapshots' manager script*

== *Code* ==

{{{


#!/bin/sh

set -e
# Script to manage snapshots with btrfs fs
# All snapshots are created in rw mode
# btrfs subvol list -o 		// choose path//
# btrfs subvol list / 		// list all //
# btrfs subvol snapshot / /.snapshots/snapshot-name   //create a snapshot //
# btrfs subvol snapshot delete <ID>  // delete snapshot by ID //



# vars
snapdir="/.snapshots"
snapori="/"


ListAll() {
	printf "\nSubvol And Snapshots list:\n\n$(doas btrfs subv list -t $snapori)" 
}

ListSnap() {
	printf "\nSnapshots List : \n\n$(doas btrfs subv list -t -o -s $snapdir)\n" 
}

CreateSnap() {

	dt="$(date +%y-%m-%d)"
	name="Linux$(uname -r | cut -d '_' -f 1)"
	doas btrfs subv snapshot / $snapdir/$name-$dt
}

DeleteSnap() {

	printf "\nAvailables Snapshots: \n$(doas btrfs subv list -o -s $snapdir | awk -F '/' '{print $2}')\nChoose Snapshots to Delete:  " 
	read snap
	printf "\nDeleting Snapshot $snap \n$(doas btrfs subv delete $snapdir/$snap)\n"

}

help() {
printf "\n\n\n╭────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮\n\n\n"
	printf "\n\t\t\t\t\t\t\t\t\t\t----- Help-----\n\n\n\t\t1.- We assumed that all your snapshots are in a @snapshot mount in /.snapshots,Edit the variables to chage the Paths.\n\t\t2.- The dt var that add the date to snapshots name could be edited as well.\n\t\t3.- This scripts uses doas as replacement of sudo, edit it if you uses sudo instead.\n "

printf "\n\n╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯\n"
}

menu() {
	# list options
	printf "\n\n\n\t\t---------------- Visnapper ---------------\n\n\t  Script to manage subvol and snapshots in a btrfs system\n"
	printf "1 --> List all\n2 --> List snapshots\n3 --> Create snapshot\n4 --> Delete snapshot\n5 --> Help\n6 --> Exit\n\nChoose and Option:\n"
	read   op
	case "$op" in

		1) 
			ListAll 
			menu ;;

		2) 
			ListSnap
			menu ;;

		3) 
			CreateSnap
			ListSnap
			menu ;;

		4) 
			DeleteSnap 
			ListSnap
			menu ;;
	
		5) 
			clear && help 
			menu ;;

		6) 	
			exit ;;

	esac

}

menu

  }}}
