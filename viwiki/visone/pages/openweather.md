
= *Dwmblocks Openweather Script* =

 _Visone Weather Script_

== Description ==

This script allows you to show the weather temperature in your dwmblock in your dwmblock bar.

The icon can be set in dwmblock config
	
== Dependencies == 

*  jq  


== Code: ==

{{{

#!/bin/sh*

KEY="756edce7e9d4c385ef9499a53492678c"
CITY="2522258"
UNITS="metric"
SYMBOL="°C"

API="https://api.openweathermap.org/data/2.5"

if [ -n "$CITY" ]; then
    if [ "$CITY" -eq "$CITY" ] 2>/dev/null; then
        CITY_PARAM="id=$CITY"
    else
        CITY_PARAM="q=$CITY"
    fi

    current=$(curl -sf "$API/weather?appid=$KEY&$CITY_PARAM&units=$UNITS")
    forecast=$(curl -sf "$API/forecast?appid=$KEY&$CITY_PARAM&units=$UNITS&cnt=1")
else
    location=$(curl -sf https://location.services.mozilla.com/v1/geolocate?key=geoclue)

    if [ -n "$location" ]; then
        location_lat="$(echo "$location" | jq '.location.lat')"
        location_lon="$(echo "$location" | jq '.location.lng')"

        current=$(curl -sf "$API/weather?appid=$KEY&lat=$location_lat&lon=$location_lon&units=$UNITS")
        forecast=$(curl -sf "$API/forecast?appid=$KEY&lat=$location_lat&lon=$location_lon&units=$UNITS&cnt=1")
    fi
fi

if [ -n "$current" ] && [ -n "$forecast" ]; then
    current_temp=$(echo "$current" | jq ".main.temp" | cut -d "." -f 1)

    forecast_temp=$(echo "$forecast" | jq ".list[].main.temp" | cut -d "." -f 1)



    echo "└ $current_temp$SYMBOL ┘ "
    
fi

}}}
