
= *Dwmblocks Fan Script* =


== Description ==

This script allows you to show the fans speed in your dwmblock bar.
The icon can be set in dwmblock config


== Options ==

*  Can remove or add more fans lectures following the same format


== Dependencies ==

*  lm-sensors


== Code: ==

{{{

#!/bin/bash

speed=$(sensors | grep fan2 | awk '{print $2; exit}')
speed1=$(sensors | grep fan1 | awk '{print $2; exit}')


if [ "$speed" != "" ] && [ "$speed1" != "" ] ; then
    speed_round=$(echo "scale=1;$speed/1000" | bc -l )
    speed_round1=$(echo "scale=1;$speed1/1000" | bc -l)
    echo "└ $speed_round / $speed_round1 ┘"
else
   echo "└ # ┘"
fi

}}}
