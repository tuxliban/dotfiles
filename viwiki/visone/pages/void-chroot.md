
= *Chroot musl UEFI installation process* =

== Disks partition ==

-  Crerate the filesystems.

	- # mkfs.vfat -F32 /dev/$boot
	- # mkfs.ext4 /dev/$root

- Mount both partitions.

	- # mount /dev/$root /mnt
	- # mkdir -p /mnt/boot/efi
	- # mount dev/$boot /mnt/boot/efi


=== BTRFS FS option ===
	

=== Add this pkgs to install ===

-	btrfs-progs grub-btrfs


=== Formating and mounting ===

- Crerate the filesystems.

	- # mkfs.vfat -F32 /dev/$boot
	- # mkfs.btrfs /dev/$root

- Creating de subvolumes and mounting partitions

	- # BTRFS_OPTS="rw,noatime,ssd,compress=zstd,space_cache=v2,commit=120"
	- # mount -o $BTRFS_OPTS /dev/$root /mnt
	- # btrfs subvolume create /mnt/@
	- # btrfs subvolume create /mnt/@home
	- # btrfs subvolume create /mnt/@snapshots
	- # umount /mnt
	- # mount -o $BTRFS_OPTS,subvol=@ /dev/$root /mnt
	- # mkdir -p /mnt/{.snapshots,home,var/cache,boot/efi}
	- # mount /dev/$boot /mnt/boot/efi
	- # mount -o $BTRFS_OPTS,subvol=@snapshots /dev/$root /mnt/.snapshots
	- # mount -o $BTRFS_OPTS,subvol=@home /dev/$root /mnt/home
	- # btrfs subvolume create /mnt/var/cache/xbps
	- # btrfs subvolume create /mnt/var/tmp
	- # btrfs subvolume create /mnt/srv

	
=== Base Installation    XBPS Method  === 

- Setting mirror, arquitecture, packages variables, C library.

	* Glibc
	
	- # REPO=https://alpha.de.repo.voidlinux.org/current
	- # ARCH=x86_64
	

	* Musl

	- # REPO=https://alpha.de.repo.voidlinux.org/current/musl
	- # ARCH=x86_64-musl


	* Both (This pkgs are my personal opcion for a lightweigh installation, edit the variable $V_PKGS por your personal options ) 
	  
	- # PKG="base-files>=0.77 ncurses coreutils findutils diffutils libgcc mksh grep gzip file sed gawk less util-linux which tar bsdtar mdocml>=1.13.3 shadow dosfstools procps-ng tzdata pciutils usbutils iana-etc openssh kbd iproute2 iputils wpa_supplicant xbps opendoas traceroute ethtool kmod eudev runit-void removed-packages ca-certificates dracut linux5.13 linux5.13-headers linux-firmware-amd linux-firmware-network make gcc pkg-config patch grub-x86_64-efi dhcpcd neovim"

### Personal dwm instalation pkgs

	- # V_PKG="$PKG xorg-minimal xrdb lm_sensors cifs-utils xf86-video-amdgpu mesa-dri mesa-vaapi mesa-vdpau samba udevil pulseaudio libX11-devel libXft-devel libXinerama-devel freetype-devel fontconfig-devel harfbuzz-devel imlib2-devel giflib-devel libexif-devel virt-manager libvirt qemu mpv ranger xclip ffmpeg rofi transmission-gtk pywal git font-awesome5 zsh zsh-syntax-highlighting zsh-autosuggestions qutebrowser python3-adblock imlib2-devel giflib-devel libexif-devel"
	
- Installing PKGS
	- # XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" "$V_PKG"


=== Configuration ===

- Entering the Chroot

	- Mount the pseudo-filesystems

		- # for i in sys dev proc; do $(mount --rbind /$i /mnt/$i && mount --make-rslave /mnt/$i); done
	
	- DNS configuration

		- # cp /etc/resolv.conf /mnt/etc/

	- Chroot into the new installation
	
		- # PS1='(chroot) # ' chroot /mnt/ /bin/bash

		- Chroot Btrfs 
	  
	  		- # BTRFS_OPTS=$BTRFS_OPTS PS1='(chroot) # ' chroot /mnt/ /bin/bash
		
	
	- Specify the hostname

		- (chroot) # echo "$user-musl" >> /etc/hostname
		
	- Configure rc.conf rc.local
	
	- Setting users and passwd
		
		* Set root passwd
			- (chroot) # passwd 
		* Set user groups and passwd 
			- (chroot) # useradd -m -G users,video,audio,network,storage,xbuilder,input $user
			- (chroot) # passwd $user
	
=== Creating Btrfs fstab ===

	- # touch /mnt/etc/fstab
	- # (chroot) # UEFI_UUID=$(blkid -s UUID -o value /dev/$boot)
	- # (chroot) # ROOT_UUID=$(blkid -s UUID -o value /dev/$root)
	- # (chroot) #  cat <<EOF > /etc/fstab
	 UUID=$ROOT_UUID / btrfs $BTRFS_OPTS,subvol=@ 0 1
	 UUID=$UEFI_UUID /efi vfat noatime 0 2
	 UUID=$ROOT_UUID /home btrfs $BTRFS_OPTS,subvol=@home 0 2
	 UUID=$ROOT_UUID /.snapshots btrfs $BTRFS_OPTS,subvol=@snapshots 0 2
	 tmpfs /tmp tmpfs noatime,nosuid,nodev 0 0
	 EOF
	
=== Creating Ext4 fstab ===
	
		- # touch /mnt/etc/fstab
		- # (chroot) # UEFI_UUID=$(blkid -s UUID -o value /dev/boot)
		- # (chroot) # ROOT_UUID=$(blkid -s UUID -o value /dev/root)
		- # (chroot) # cat <<EOF > /etc/fstab
		UUID=$UEFI_UUID /boot/efi vfat noatime 0 0
		UUID=$ROOT_UUID / ext4 noatime 0 0
		tmpfs           /tmp        tmpfs   noatime,nosuid,nodev   0 0
		EOF
		
	- Configuring dracut.conf
	
		- (chroot) # echo "hostonly=yes" /etc/dracut.conf

	- Installing grub
	
		(chroot) # grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="$user-musl"	
		
	- Reconfigure pkgs
	
		(chroot) # xbps-reconfigure -fa
		
		
