
= *Write Node Script* =

 _Visone Write Node With Mouse Script_


== Description ==

This script allows you to paint the geometry of the new node with de mouse  and make a new node with the aplicacions you want
	  

== Usage ==

* Check [[bspwmrc-conf|Bspemrc Confg]] for examples

	
== Dependencies == 

*  Slot
 

== Code: ==
 
 {{{
 
 
 
#!/bin/bash
  
 # Draw a floating kitty
  # Inspired by u/f0x52
 
  # Draw a rectangle using slop then read the geometry     value
  #read -r X Y W H < <(slop -f "%x %y %w %h" -b 1 -t 0     -q)
	slop=$(slop -f "%x %y %w %h") || exit 1
  read -r X Y W H < <(echo $slop)
 
  # Create a variable to be used for term flag option
  g=${W}x${H}+${X}+${Y}


 
  # Draw with floating rule

bspc rule -a "*" -o  state=floating rectangle="$g"
#bspc rule -a kitty -o  state=floating rectangle="$g"
#bspc rule -a Pulseaudio-equalizer-gtk -o  state=floating rectangle="$g"
#bspc rule -a Brave-browser -o  state=floating rectangle="$g"






#op="Terminal\nRanger\nCava\nMoc\nNeofetch\nRec-Dsktp"
#
#pr="$(echo -e "$op" | rofi -dmenu -i -l 4 -p "Launch Options")"
#
# 	case "$pr" in
# 		Terminal) kitty & ;;
# 		Ranger) kitty -e ranger & ;;
# 		Cava)kitty -e cava  & ;;
# 		Moc) kitty -e "padsp mocp" & ;;
# 		Neofetch) kitty -e neofetch & ;;
# 		Rec-Dsktp) kitty -e red-dsktp ;;
# 	esac
 	
 	


#Version 2
##!/usr/bin/env bash
#read -r X Y W H < <(slop -f "%x %y %w %h" -b 1 -t 0 -q)
## Width and Height in px need to be converted to columns/rows
## To get these magic values, make a fullscreen st, and divide your screen width by ${tput cols}, height by ${tput lines} 
#(( W /= 8 ))
#(( H /= 16 ))
#g=${W}x${H}+${X}+${Y}
#bspc rule -a st-256color -o state=floating
#if [ "$1" == "perl6" ]; then
#    st -g $g -e perl6
#else
#    st -g $g
#fi




 

##!/bin/sh
##
## draw - spawn a terminal in a preselected area
#
#_() bspc config "$@"
#
#b=$(_         border_width)
#c=$(_ focused_border_color)
#
#hacksaw -s "$b" -c "$c" -n 2> /dev/null | {
#    IFS=+x read -r w h x y
#
#    [ "$w" ] || exit
#
#    # adjust geometry
#    w=$((w - 2 * b))
#    h=$((h - 2 * b))
#
#    [ $((w < 10 && h < 10)) -eq 1 ] && {
#        w=300
#        h=200
#    }
#
#    # make the newly spawned window floating & w/ the right geometry
#    bspc rule -a '*' -o state=floating rectangle="${w}x${h}+${x}+${y}"
#
#}
 
 }}}
