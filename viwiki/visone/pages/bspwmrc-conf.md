
= *Bspwm Configuracion* =

	_Visone Bspwm Conf_

	_bspwmrc file_

== Code: ==

{{{


#! /bin/sh

#### AUTOSTART ####

xsetroot -cursor_name left_ptr &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
dunst &
sxhkd &
picom -b &
numlockx on &
polybar vbspwm &
random-pywal
pgrep bspswallow || bspswallow &


#### MONITORS ####

bspc monitor HDMI-0 -d 1 2 3 4 5


#### BSPWM Configuration ####

bspc config border_radius                8
bspc config border_width                  4
bspc config window_gap                    10
bspc config top_padding                   24
bspc config bottom_padding                0
bspc config left_padding                  0
bspc config right_padding                 0
bspc config single_monocle                false
bspc config click_to_focus                false
bspc config split_ratio                   0.50
bspc config borderless_monocle            true
bspc config gapless_monocle               true
bspc config focus_by_distance             true
bspc config focus_follows_pointer         true
#bspc config history_aware_focus           true
bspc config remove_disabled_monitors      true
#bspc config merge_overlapping_monitors    true
bspc config pointer_modifier mod4
bspc config pointer_action1 move
bspc config pointer_action2 resize_side
bspc config pointer_action3 resize_corner


##### BSPWM Coloring ####

# source the colors.
. "${HOME}/.cache/wal/colors.sh"

# Set the border colors.
bspc config normal_border_color "$color1"
bspc config active_border_color "$color9"
bspc config focused_border_color "$color5"



##### BSPWM Rules ####

bspc rule -a MEGAsync desktop='^3' state=floating follow=on
bspc rule -a qBittorrent desktop='^5'
bspc config external_rules_command ~/.config/bspwm/scripts/floating.sh


}}}}
