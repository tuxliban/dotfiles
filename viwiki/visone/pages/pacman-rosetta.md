
= *Pacman Rosetta* =

_*Some frequently used pacman commands*_ 


== Basic Operations ==

_*This applications need root permissions*_
	
==== Install a package/s (pgk/s) by name ====
	
`pacman -S pgk/s`


==== Remove a pkg/s by name ====

`pamcan -Rs pgk/s`


==== Search pkg/s by name,description ====
	
`pacman -Ss pkg/s`


==== Upgrade & Update pkg/s already isntalled ====

`pacman -Syu`


==== Clean up all caches ====

`pacman -Sc/c`


==== Remove dependencies no longer needed ====

`pacman -Qdtq | pacman -Rs -`


==== Download pkg/s only ====

`pacman -Sw`


==== Install local pkg/s ====

`pacman -U pkg/s`


==== Show info of a pkg remote/local ====

`pacman -Si/-Qi pkg``   


==== Remove pkgs cache. N = verions of pkg/s left ====
	
_needs pacman-contrib_

`paccache -rkN`


 

