
== *Qemu & Virt-Manager* ==

_Virtualization software_


=== *Installation* ===


* Needed pkgs

`sudo pacman -S --needed qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables iptables`        


*  Libguestfs    

_libguestfs is a set of tools used to access and modify virtual machine (VM) disk images. You can use this for viewing and editing files inside guest, scripting changes to VM's,monitoring disk use/free statistics,creating guests,P2v,V2v,performance backup,etc_    

`yay -S --needed libguestfs --noconfirm`    


*  Enable services SystemD    

`sudo systemctl enable --now libvirtd.service`    


*  Enable services Runit    

`sudo ln -s /etc/runit/sv/libvirtd /run/runit/service/libvirtd`    

`sudo ln -s /etc/runit/sv/virtlogd /run/runit/service/virtlogd`    

`sudo ln -s /etc/runit/sv/virtlockd /run/runit/service/virtlockd`    


*  Enable $USER account    

_Open with your $EDITOR_ `/etc/libvirt/libvirtd.conf` _with root permissions to edit it_    


1.  Set UNIX domain socket grput to libvirt    

`unix_sock_group = libvirt`    


2.  Set UNIX socket permissions for RW      

`unix_sock_rw_perms = "0700" `     


3.  Add your user account to libvirt group    

`sudo usermod -a -G libvirt $(whoami)`    

`newgrp libvirt`    


*  Restart services SystemD    

`sudo systemctl restart libvirtd`    


* Restart services Runit    

`sudo sv restart libvirtd`    

`sudo sv restart virtlogd`    

`sudo sv restart virtlockd`    
