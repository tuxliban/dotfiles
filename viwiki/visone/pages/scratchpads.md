
= *Visone Scratchpads Script* =

*WM agnostic script to create/manage scratchpads*

== *Code* ==

{{{

#! /bin/sh

# Usage: 
# scratchpads foo  # will launch foo in a scratchpad, run the command again
#  to toggle it


SCRATCHPAD_NAME=$1
[ -z $SCRATCHPAD_NAME ] && SCRATCHPAD_NAME="sctpad"
case "$1" in

	scratchpad) 		entry="term-launcher -s" 		;;
	tg)			entry="term-launcher -g"		;;
	pulsemixer) 		entry="term-launcher -p "		;;
	float) 			entry="term-launcher -r "		;;
	rxvt) 			entry="term-launcher -s "		;;
	firefox)		entry="firefox "			;;
	qutebrowser) 		entry="qutebrowser "			;;
	*)			entry="urxvtc -name $SCRATCHPAD_NAME "  ;;
esac


	xdotool search --onlyvisible --classname $SCRATCHPAD_NAME windowunmap \
      || xdotool search --classname $SCRATCHPAD_NAME windowmap \
      || $entry &


  }}}
