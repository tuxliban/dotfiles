
== * Xbps-Dmenu Shell Manager Functions* ==

_*This functions are written to add them to your shell config file*_

_*This functions will allow you to install, uninstall, search and update your void-linux installation*_


=== *Dependencies:* ===

*  Dmenu (Xresources patch for colors, or edit the dmenu command)
*  Opendoas (if you uses sudo just edit the functions and change doas to sudo)


=== *Code:* ===

{{{


ins() { 
	
	xbps-query -Rs '*' | awk '{print $2}' | dmenu -nb "$color10" -nf "$color0" -sb "$color14" -sf "$color0" -fn "Source Code Pro-10" -p "Select package(s) to install: " | xargs -ro doas xbps-install -Sy
}


del() {


	xbps-query -m | dmenu -nb "$color10" -nf "$color0" -sb "$color14" -sf "$color0" -fn "Source Code Pro-10" -p "Select package(s) to remove: " | xargs -ro doas xbps-remove -Ro 
}

up() {

	
	pkg=$(xbps-install --memory-sync --dry-run --update | grep -Fe update -e install | awk '{print $1}')
	if [ $(printf "$pkg" | wc -L) -ge 1 ]
	then
	upk=$(printf "$pkg" | dmenu -nb "$color10" -nf "$color0" -sb "$color14" -sf "$color0" -fn "Source Code Pro-10" -p "Void package(s) to update, Update them? y/n")
	if [ "$upk" = y ]
	then
		doas xbps-install -Syu
		# Remove Orphans
		doas xbps-remove -oO 
		# Clean cache
		doas rm -r /var/cache/xbps/*
	else
		exit
	fi
	else
	printf "Nothing to Update" | dmenu -nb "$color10" -nf "$color0" -sb "$color14" -sf "$color0" -fn "Source Code Pro-10" -p "Void package(s) to update: "
	fi
}


  }}}
