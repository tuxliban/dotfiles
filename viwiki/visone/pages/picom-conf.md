
= *Picom Jonaburg Conf* =

	_Visone Picom-Jonaburg conf_

=== Code: ===

{{{

### Animations

transition-length = 300
transition-pow-x = 0.1
transition-pow-y = 0.1
transition-pow-w = 0.1
transition-pow-h = 0.1
size-transition = true
#spawn-center-screen = false
#spawn-center = false
#no-scale-down =false

### Corners


corner-radius = 8.0;
rounded-corners-exclude = [
  #"window_type = 'normal'",
  "class_g = 'dwm'",
];
round-borders = 2;
round-borders-exclude = [
  "class_g = 'dwm'",
];



### Fading              

fading = true;
fade-in-step = 0.03;
fade-out-step = 0.03;
fade-delta = 15
# fade-exclude = []
no-fading-openclose = false
no-fading-destroyed-argb = false


###   Transparency / Opacity      

inactive-opacity = 1;
frame-opacity = 0.8;
inactive-opacity-override = false;
active-opacity = 1.0
#inactive-dim = 1
#inactive-dim-fixed = 1.0
opacity-rule = [
 	"100:class_g	= 'TelegramDesktop'",
 	"80:class_g	= 'st-256color'",
	"80:class_g	=  'nvim'",
	"80:class_g	=  'tabbed'",
	"85:class_g	=  'Rofi'",
 ];




### Background-Blurring-picom      

#blur-size = 12
#blur-deviation = false
#blur-strength = 5
#blur-background = true
#blur-background-frame = true 
#blur-background-fixed = false
#blur-kern = "3x3box";
#blur-background-exclude = [
#  "window_type = 'dock'",
#  "window_type = 'desktop'",
#  "_GTK_FRAME_EXTENTS@:c"
#];


### Background-Blurring

blur: {
  # requires: https://github.com/ibhagwan/picom
  method = "kawase";
  #method = "kernel";
  strength = 3;
  # deviation = 1.0;
  # kernel = "11x11gaussian";
  background = false;
  background-frame = false;
  background-fixed = false;
  kern = "3x3box";
}

# Exclude conditions for background blur.
blur-background-exclude = [
  #"window_type = 'dock'",
  #"window_type = 'desktop'",
  #"class_g = 'URxvt'",
  #
  # prevents picom from blurring the background
  # when taking selection screenshot with `main`
  # https://github.com/naelstrof/maim/issues/130
  "class_g = 'scrot'",
  "_GTK_FRAME_EXTENTS@:c"
];

## General Settings        

backend = "glx";
vsync = true;
mark-wmwin-focused = true;
mark-ovredir-focused = true;
detect-client-opacity = true;
refresh-rate = 0;
# use-ewmh-active-win = false
# unredir-if-possible = false
# unredir-if-possible-delay = 0
detect-transient = true;
detect-client-leader = true;
#resize-damage = 1
glx-no-stencil = true 
glx-no-rebind-pixmap = false 
#use-damage = true;
xrender-sync-fence = true 
#transparent-clipping = false
log-level = "warn";


# 'WINDOW_TYPE' is one of the 15 window types defined in EWMH standard:
#     "unknown", "desktop", "dock", "toolbar", "menu", "utility",
#     "splash", "dialog", "normal", "dropdown_menu", "popup_menu",
#     "tooltip", "notification", "combo", and "dnd".
wintypes:
{
  tooltip = { fade = true; shadow = true; opacity = 0.75; 
  		focus = true; full-shadow = false; };
  dock = { shadow = false; blur= false; }
  dnd = { shadow = false; }
  popup_menu = { opacity = 0.8; }
  dropdown_menu = { opacity = 0.8; }
};

}}}
