
= *NNN* =

*Visone nnn enviroment variables*

== *Code* ==

{{{


# nnn Plugins

NNN_PLUG_DIR='x:visone/vdelete-c;X:visone/vdelete-s;D:visone/mdownloads;T:visone/mtv-shows;O:visone/mothers;F:visone/mfilms;S:visone/mseries;v:diffs'
NNN_PLUG_FILE='i:visone/viewer;z:-!devour mpv $nnn*'
NNN_PLUG_VARIOS='f:fixname;d:dragdrop'
NNN_PLUG_NVIM='e:-!nvim $nnn*'
NNN_PLUG="$NNN_PLUG_DIR;$NNN_PLUG_FILE;$NNN_PLUG_VARIOS;$NNN_PLUG_NVIM"
export NNN_PLUG

# nnn Bookmarks

export NNN_BMS='w:~/wallpapers;d:/media/Datos/Downloads;t:/media/Datos/Downloads/Torrenting;s:~/scripts;o:/media/Datos/Varios/.others;s:/media/Datos/Varios/Series;f:/media/Datos/Varios/Films;t:/media/Datos/TV-Shows/'

# nnn options 
export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
export NNN_OPENER=/home/visone/.config/nnn/plugins/visone/vi-nuke
export NNN_USE_EDITOR=1                   
export NNN_OPTS="SceEuo"
export NNN_FIFO=/tmp/nnn.fifo 
export PAGER="less"               
export EDITOR="nvim"              
export VISUAL="$PAGER"               
export IMAGEVIEWER="sxiv" 
export FILE="nnn"
export BROWSER='qutebrowser'
export TERM="urxvtc"
export LANG=es_ES.UTF-8
export MENU="dmenu"
export sel=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection


# nnn colors

BLK="0B" CHR="0B" DIR="04" EXE="06" REG="00" HARDLINK="06" SYMLINK="06" MISSING="00" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"





  }}}
