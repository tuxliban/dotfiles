
= *Bsp Layout Config* =
 
  _Visone Bsp Layout Config_

== Dependencies ==  

*  bsp-layout
  
  `layoutrc file`
   `path ~/.config/bspwm/bspc-layout/layoutrc`


== Code: ==


{{{


# Tall layout config
TALL_RATIO=0.5;

# Wide layout config
WIDE_RATIO=0.5;



}}}

=*Bspwm Layout Notification Script*=

 _Visone Bspwm Notification Script_
 
 	* It show you a nitification any time
	you changes layouts with the desktop name
	and the actual layout
	
	* Dependencies dunst
 
 {{{
 
 
#!/bin/bash

dk=$(bspc query --names -D -d focused)
ly=$(bsp-layout get "$dk" )

dunstify "Bspwm-Layout" " Desktop $dk\n layout $ly" 
 
 
 
 }}}
