
= *Dwmblocks Vol Script* =

 _Visone vol script for Dwmblocks_


== Description ==

This scrip allow you to show the % volume valor in dwmblocks bar.  

The icon can be set in dwmblocks config

== Dependencies ==  

*  Pulsemixer
		
=== Code: ===
	
{{{
#!/bin/bash 

vol="$(pulsemixer --get-volume | awk '{print $1}')"

echo "└ ${vol}% ┘"

}}}
