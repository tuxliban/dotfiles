

== *Visone Manuals* ==


=== *Manuals* ===

*   [[pages/iwctl|Iwctl manual]] 
*   [[pages/guia-nvidia|Guia Nvidia Drivers Manjaro ]] 
*   [[pages/guia-swap|Guia Swap Config]] 
*   [[pages/pacman-rosetta|Pacman Rosetta]]
*   [[pages/reinstall-grub|Grub Reinstalation (Arch Based Distros)]]
*   [[pages/qemu-virt-manager|Quemu & Virt-Manager]]
*   [[pages/bash-init|Using Bash as Init (Changing Passwords)]]
*   [[pages/void-chroot|Chroot Void-Install Method ]]
*   [[pages/dracut-shell|Dracut emergency shell ]]
    


=== *Ffmpeg Commands* ===

*   [[pages/ffmpeg-vaapi|Ffmpeg Transcode vaapi-hevc]] 
*   [[pages/ffmpeg-hevc|Ffmpeg Transcode libx265]] 
*   [[pages/ffmpeg-record|Ffmpeg Desktop Record]] 
	
	
=== *Enable vaapi WebBrowsers* ===

*   [[pages/brave-vaapi|Brave Flags to Edit]]
*   [[pages/qutebrowser|Qutebrowser Flags Options]]
*   [[pages/ff-vaapi|Firefox vaapi flags]]  



=== *Pulsemixer Sound Manager Script Manual* ===

*   [[pages/pulsemixer-controls|Pulsemixer Controls]]  
*   [[pages/pulsemixer-options|Pulsemixer Options]]  
*   [[pages/pulsemixer-usage|Pulsemixer Usage]]  
*   [[pages/pulsemixer-config|Pulsemixer Config]]  
*   [[pages/pulsemixer-examples|Pulsemixer Examples]]  
			

	
== *Configurations Files & Scripts* ==


=== *No WM specific configs* ===
		
*  [[pages/mpv-conf|MPV-Conf]] 
*  [[pages/picom-conf|Picom Jonaburg Conf]] 
*  [[pages/dunst-conf|Dunst Conf]] 
*  [[pages/rc-conf|Ranger rc-conf]] 
*  [[pages/rifle-conf|Ranger rifle-conf]] 
*  [[pages/nnn|NNN config]]
*  [[pages/dragon|Dragon - Drag & Drop cli script]]
*  [[pages/rofi-conf|Rofi Config Rasi]] 
*  [[pages/20-amdgpu-conf|X Server AMDGPU Config]] 
*  [[pages/samba-config-station|Samba Config Station]] 
*  [[pages/pacman-conf-station|Pacman Conf Station]] 
*  [[pages/makepkg-conf-station|Makepkg Conf Station]] 
*  [[pages/udev-rules|Udev Rules SSD/M2/HDD]] 
*  [[pages/zshrc|Zsh Conf]] 
*  [[pages/mkshrc|Mksh Conf]] 
*  [[pages/aliases|Shell Aliases]] 
*  [[pages/urxvt|URxvt config]] 
*  [[pages/bsdtar-func|Bsdtar extract/compress functions]]
*  [[pages/xbps-dmenu|Dmenu-xbps shell manager]]

		
=== *General Scripts* ===

*  [[pages/rofi-usb|Rofi-Usb]] 
*  [[pages/rofi-wifi|Rofi-Wifi]] 
*  [[pages/ytdl-mpv|Ytdl-Mpv]] 
*  [[pages/media-editor|Media-Editor]] 
*  [[pages/runit-manager|Runit Service Manager]]
*  [[pages/change-governor|Change CPU governor]]
*  [[pages/alias-search|Alias-Search]]
*  [[pages/bright|Brightness changer script]]
*  [[pages/visnapper|Btrfs snapshots' manager]]
*  [[pages/udevil|Udevil mounter script--Beta]]
*  [[pages/visfeed|Sfeed script to manage RSS feeds]]
*  [[pages/vyts|Youtube Searcher Script]]
*  [[pages/vifetch|Visone fetch]]
*  [[pages/scratchpads|Visone Scratchpads script]]


=== *Dwm Scripts* ===
		
*  [[pages/volume-script|Volume Script]] 
*  [[pages/dwm-screenshots|Dwm-Screenshots]] 
*  [[pages/dwm-scrot|Dwm-scrot]] 
*  [[pages/tabbed-st-ranger|Tabbed st ranger]] 
*  [[pages/st-launcher|St terminal launcher]]
*  [[pages/urxvt-launcher|URxrvt launcher]]

==== *Dwmblocks Configs & Scripts* ====

*  [[pages/vol|Volume]] 
*  [[pages/openweather|Openweather]] 
*  [[pages/cputemp|Cputemp]] 
*  [[pages/fan|Fan]] 
*  [[pages/clock|Clock]] 
*  [[pages/pacupdate|PacUpdate]] 
*  [[pages/system|System]] 
*  [[pages/blocks-h|Blocks.h]] 

=== *Slstatus Config* ===
*  [[pages/slstatus|Slstatus config]] 
				

=== *Bspwm Configs & Scripts* ===

*  [[pages/bspwmrc-conf|BspwmRc Conf]] 
*  [[pages/sxhkdrc-conf|SxhkdRc Conf]] 
*  [[pages/polybar-conf|Polybar Conf]] 
*  [[pages/bsp-layout-conf|Bsp-Layout Conf]] 
==== *Bspwm Scripts* ====

*  [[pages/bspswallow-script|Bspswallow Script]] 
*  [[pages/bspwm-scratchpad-script|Bspwm Scratchpad Script]] 
*  [[pages/write-node-script|Write Node Script]] 

		


















